﻿using UnityEngine;
using UnityEngine.UI; 
using System.Collections;
using System; 

public class ScoreboardAnimationHandler : MonoBehaviour {

	Animator animator; 
	GameFlow game; 
	LostBall lostBall; 

	public GameObject parent; 

	public int ballLost; 
	public int camel; 
	public int doubleUp; 
	public int gecko; 
	public int maxwell; 
	public int multiBall; 
	public int multiplier; 
	public int pinballLogo; 

	bool animating=false; 

	AnimatorStateInfo animationState; 

	public Collider camelCollider; 
	public Collider maxwellCollider; 
	public Collider geckoCollider; 

	#region Monobehaviour
	// Use this for initialization
	IEnumerator Start () {


		FindComponents (); 
		SetAnimatorHash ();
		animator.enabled=true; 
		yield return new WaitForSeconds (0.5f); 

		StartCoroutine("DoLogoAnimation"); 
		
	}
	

	void Update()
	{		
		animationState = animator.GetCurrentAnimatorStateInfo (0); 

	}
	#endregion

	public IEnumerator DoLogoAnimation()
	{

		if (!animating) {
			animator.Play (pinballLogo); 
		} 
		yield return new WaitForSeconds (2f); 
		StartCoroutine (DoLogoAnimation ()); 

	}

	public IEnumerator Animate(int animHash)
	{
		StopCoroutine ("DoLogoAnimation"); 
		animating = true; 
		if (!parent.activeInHierarchy)
			parent.SetActive (true); 

		animator.enabled = true; 
		animator.Play (animHash);
		animator.Update (0); 
		yield return new WaitForEndOfFrame(); 
		if(animationState.length>0){
			yield return new WaitForSeconds (animationState.length);
		}
		else{
			animator.Update (0);
			yield return new WaitForEndOfFrame(); 
			yield return new WaitForSeconds(animationState.length); 
		}

		animator.enabled = false; 
		parent.SetActive (false); 
		animating = false; 

	}

	public void SelectAnimation(Collider collider)
	{

		if (collider == camelCollider) 
		{
			StartCoroutine(Animate (camel)); 
		}
		else if (collider == geckoCollider) 
		{
			StartCoroutine(Animate (gecko)); 
		}
		else if (collider == maxwellCollider) 
		{
			StartCoroutine(Animate (maxwell)); 
		}
	}

	public void DisableAnimation()
	{
		animator.enabled = false; 
		parent.SetActive (false); 
	}


	void FindComponents()
	{
		animator = GameObject.Find ("ScoreboardAnimation").GetComponent<Animator> (); 
		parent = this.parent; 
		game = GameObject.Find ("GameFlow").GetComponent<GameFlow> (); 
		lostBall = GameObject.Find ("triggerBallLost").GetComponent<LostBall> (); 
		camelCollider = GameObject.Find ("triggerCamel01").GetComponent<Collider> (); 
		geckoCollider = GameObject.Find ("triggerGecko01").GetComponent<Collider> (); 
		maxwellCollider = GameObject.Find ("triggerRabbit01").GetComponent<Collider> (); 
	}

	void SetAnimatorHash()
	{
		ballLost = Animator.StringToHash ("Base Layer.BallLostAnimation"); 
		camel = Animator.StringToHash ("Base Layer.CamelHumpDay"); 
		doubleUp = Animator.StringToHash ("Base Layer.DoubleUp"); 
		gecko = Animator.StringToHash ("Base Layer.Gecko"); 
		maxwell = Animator.StringToHash ("Base Layer.Maxwell"); 
		multiBall = Animator.StringToHash ("Base Layer.Multiball"); 
		multiplier = Animator.StringToHash ("Base Layer.Multiplier"); 
		pinballLogo = Animator.StringToHash ("Base Layer.PinballLogo"); 
	}


}
