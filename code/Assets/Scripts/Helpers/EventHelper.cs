﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class EventHelper {

	static public void AddEventTrigger(GameObject go, EventTriggerType triggerType, System.Action action)
	{
		EventTrigger trig = go.GetComponent<EventTrigger> ();
		if (trig == null)
			trig = go.AddComponent<EventTrigger> ();
		if (trig.triggers == null)
			trig.triggers = new System.Collections.Generic.List<EventTrigger.Entry> ();
		AddEventTrigger (trig, triggerType, action);
	}

	static public void AddEventTrigger(EventTrigger eventTrigger, EventTriggerType triggerType, System.Action action)
	{
		// Create a nee TriggerEvent and add a listener
		EventTrigger.TriggerEvent trigger = new EventTrigger.TriggerEvent();
		trigger.AddListener((eventData) => action()); // ignore event data
		
		// Create and initialise EventTrigger.Entry using the created TriggerEvent
		EventTrigger.Entry entry = new EventTrigger.Entry() { callback = trigger, eventID = triggerType };
		
		// Add the EventTrigger.Entry to delegates list on the EventTrigger
		eventTrigger.triggers.Add(entry);
	}
}
