﻿using UnityEngine;
using System.Collections;

public static class UIHelper {

	public static RectTransform GetScreenRect(RectTransform rectTrans) {
		Vector3[] corners = new Vector3[4]; 
		rectTrans.GetWorldCorners (corners); 

		float xMin = float.PositiveInfinity, xMax = float.NegativeInfinity, yMin = float.PositiveInfinity, yMax = float.NegativeInfinity;
		for (int i = 0; i < 4; ++i) {
			// For Canvas mode Screen Space - Overlay there is no Camera; best solution I've found
			// is to use RectTransformUtility.WorldToScreenPoint) with a null camera.
			Vector3 screenCoord = RectTransformUtility.WorldToScreenPoint(null, corners[i]);
			if (screenCoord.x < xMin) xMin = screenCoord.x;
			if (screenCoord.x > xMax) xMax = screenCoord.x;
			if (screenCoord.y < yMin) yMin = screenCoord.y;
			if (screenCoord.y > yMax) yMax = screenCoord.y;
			corners[i] = screenCoord;
		}
		Rect result = new Rect(xMin, yMin, xMax - xMin, yMax - yMin);
		rectTrans.anchorMin = new Vector2(result.xMin, result.yMin); 
		rectTrans.anchorMax =new Vector2(result.xMax, result.yMax); 
		return rectTrans;
	}
}
