﻿using UnityEngine;
using System.Collections;

public class ScoreTrigger : MonoBehaviour {

	private GameFlow gameFlowGC;
	public int pointsToGive = 100;
	
	void Start () {
		gameFlowGC=GameObject.Find("GameFlow").GetComponent<GameFlow>();
	}

	void OnTriggerEnter(Collider other){
		gameFlowGC.SetScorePoints(pointsToGive, this.gameObject);
	}
}
