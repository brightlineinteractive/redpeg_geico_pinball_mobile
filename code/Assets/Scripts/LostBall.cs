﻿using UnityEngine;
using System.Collections;

public class LostBall : MonoBehaviour {

	private GameFlow gameFlowGC;
	public AudioClip flipperKicks;
	public ScoreboardAnimationHandler animation; 


	void Start(){
		animation = GameObject.Find ("ScoreboardAnimationHandler").GetComponent<ScoreboardAnimationHandler> (); 
		gameFlowGC=GameObject.Find("GameFlow").GetComponent<GameFlow>();
		
	}
	void OnTriggerEnter (Collider other) 
	{
		//check if the object touching the trigger is the ball, otherwise anyother object
		//touching the trigger will make you loose the game
		if (other.CompareTag ("Ball")) { 

			StartCoroutine(animation.Animate(animation.ballLost)); 

			other.gameObject.transform.FindChild ("Trail").gameObject.SetActive (false);
			other.gameObject.SetActive (false);
			
			gameFlowGC.Respawn ();
			if (flipperKicks.isReadyToPlay) {
				AudioSource.PlayClipAtPoint (flipperKicks, transform.position, Settings.instance.soundVolume);
				
			}	


		} 
	}
	
}
