﻿using UnityEngine;
using System.Collections;

public class MovieControl : MonoBehaviour {

	public GameObject BallLostMovie;
	public GameObject CamelMovie;
	public GameObject CircleMovie;
	public GameObject GenericMovie;
	public GameObject MaxwellMovie;
	public GameObject GeckoMovie;
	
//	private AVProQuickTimeMovie CircleVideo;
//	private AVProQuickTimeMovie BallLostVideo;
//	private AVProQuickTimeMovie CamelVideo;
//	private AVProQuickTimeMovie GenericVideo;
//	private AVProQuickTimeMovie MaxwellVideo;
//	private AVProQuickTimeMovie GeckoVideo;
//			
//	private AVProQuickTime CirclePlayer;
//	private AVProQuickTime BallLostPlayer;
//	private AVProQuickTime CamelPlayer;
//	private AVProQuickTime GenericPlayer;
//	private AVProQuickTime MaxwellPlayer;
//	private AVProQuickTime GeckoPlayer;

	private uint circleLoopStartFrame = 30u;
	private uint circleLoopEndFrame = 1866u;

	private bool circleLooping = false;

	void Start () {
		StartCoroutine(DoVideoSetup());
	}
	
	private IEnumerator DoVideoSetup(){
		yield return new WaitForSeconds(0.5f);
//		if(CircleMovie){
//			CircleVideo=CircleMovie.GetComponent<AVProQuickTimeMovie>() ;
//			CircleVideo.LoadMovie();
//			CirclePlayer=CircleVideo.MovieInstance;
//		}
//		if(CamelMovie){
//			CamelVideo=CamelMovie.GetComponent<AVProQuickTimeMovie>() ;
//			CamelVideo.LoadMovie();
//			CamelPlayer=CamelVideo.MovieInstance;
//		}
//		if(BallLostMovie){
//			BallLostVideo=BallLostMovie.GetComponent<AVProQuickTimeMovie>() ;
//			BallLostVideo.LoadMovie();
//			BallLostPlayer=BallLostVideo.MovieInstance;
//		}
//		if(GenericMovie){
//			GenericVideo=GenericMovie.GetComponent<AVProQuickTimeMovie>() ;
//			GenericVideo.LoadMovie();
//			GenericPlayer=GenericVideo.MovieInstance;
//		}
//		if(MaxwellMovie){
//			MaxwellVideo=MaxwellMovie.GetComponent<AVProQuickTimeMovie>() ;
//			MaxwellVideo.LoadMovie();
//			MaxwellPlayer=MaxwellVideo.MovieInstance;
//		}
//		if(GeckoMovie){
//			GeckoVideo=GeckoMovie.GetComponent<AVProQuickTimeMovie>() ;
//			GeckoVideo.LoadMovie();
//			GeckoPlayer=GeckoVideo.MovieInstance;
//		}
	}
	
	public void StartGame(int gameLength){
//		//CircleVideo.SetActive(false);
//		CircleMovie.SetActive(true);
//		//CirclePlayer.Frame=(uint)(1+(75-gameLength)*30);
//
//		if (gameLength < 76)
//		{
//			CirclePlayer.Frame=(uint)(1+(75-gameLength)*30);
//		}
//		else
//		{
//			CirclePlayer.Frame=1u;
//			StartCoroutine("Loop");
//		}
//
//		CirclePlayer.Play();
//		GenericMovie.SetActive(true);
//		GenericVideo.Play ();
//
//		CamelMovie.SetActive(false);
//		MaxwellMovie.SetActive(false);
//		GeckoMovie.SetActive(false);
//		BallLostMovie.SetActive(false);
	}

	private IEnumerator Loop()
	{
		yield return new WaitForSeconds(0.5f); //put in by jstanley
//		circleLooping = true;
//
//		while (CirclePlayer.Frame < circleLoopEndFrame)
//			yield return null;
//
//		CirclePlayer.Frame = circleLoopStartFrame;
//		StartCoroutine ("Loop");
	}

	public void PlayCountdown()
	{
//		if (circleLooping)
//		{
//			StopCoroutine("Loop");
//		}
//
//		CirclePlayer.Pause ();
//		CirclePlayer.Frame = circleLoopEndFrame;
//		CirclePlayer.Play ();
	}

	public void EndGame(){
//		//CircleVideo.SetActive(false);
//		if (circleLooping)
//		{
//			StopCoroutine("Loop");
//		}
//
//		CircleMovie.SetActive(true);
//		CirclePlayer.Frame=2399;
//		CirclePlayer.Pause();
//		GenericMovie.SetActive(true);
//		
//		CamelMovie.SetActive(false);
//		MaxwellMovie.SetActive(false);
//		GeckoMovie.SetActive(false);
//		BallLostMovie.SetActive(false);
	}
	public void BallLost(){
//		CircleMovie.SetActive(true);
//		
//		CamelMovie.SetActive(false);
//		GenericMovie.SetActive(false);
//		MaxwellMovie.SetActive(false);
//		GeckoMovie.SetActive(false);
//		
//		BallLostMovie.SetActive(true);
//		BallLostPlayer.Frame=1;
//		BallLostPlayer.Play();
//
//		StartCoroutine (BallLostComplete ());
	}

	private IEnumerator BallLostComplete()
	{
		yield return new WaitForSeconds(3.5f);
//		GenericVideo.Play ();
//		GenericMovie.SetActive(true);
//		BallLostMovie.SetActive(false);
	}

	public void CamelTriggered(){
//		CircleMovie.SetActive(true);
//		
//		GenericMovie.SetActive(false);
//		GeckoMovie.SetActive(false);
//		MaxwellMovie.SetActive(false);
//		BallLostMovie.SetActive(false);
//		
//		CamelMovie.SetActive(true);
//		CamelPlayer.Frame=1;
//		CamelPlayer.Play();
//
//		StartCoroutine (CamelTriggeredComplete ());
	}
	private IEnumerator CamelTriggeredComplete()
	{
		yield return new WaitForSeconds(4.5f);
//		GenericVideo.Play ();
//		GenericMovie.SetActive(true);
//		CamelMovie.SetActive(false);
	}

	public void MaxwellTriggered(){
//		CircleMovie.SetActive(true);
//		
//		CamelMovie.SetActive(false);
//		GeckoMovie.SetActive(false);
//		GenericMovie.SetActive(false);
//		BallLostMovie.SetActive(false);
//		
//		MaxwellMovie.SetActive(true);
//		MaxwellPlayer.Frame=1;
//		MaxwellPlayer.Play();
//
//		StartCoroutine (MaxwellTriggeredComplete ());
	}
	private IEnumerator MaxwellTriggeredComplete()
	{
		yield return new WaitForSeconds(3.5f);
//		GenericVideo.Play ();
//		GenericMovie.SetActive(true);
//		MaxwellMovie.SetActive(false);
	}

	public void GeckoTriggered(){
//		CircleMovie.SetActive(true);
//		
//		CamelMovie.SetActive(false);
//		MaxwellMovie.SetActive(false);
//		GenericMovie.SetActive(false);
//		BallLostMovie.SetActive(false);
//		
//		GeckoMovie.SetActive(true);
//		GeckoPlayer.Frame=1;
//		GeckoPlayer.Play();
//
//		StartCoroutine (GeckoTriggeredComplete());
	}

	private IEnumerator GeckoTriggeredComplete()
	{
		yield return new WaitForSeconds(3.5f);
//		GenericVideo.Play ();
//		GenericMovie.SetActive(true);
//		GeckoMovie.SetActive(false);
	}
}
