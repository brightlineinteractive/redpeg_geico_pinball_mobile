﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 
using System.Linq; 

public class CameraControl : MonoBehaviour {

	public GameFlow gameFlow;
	public GameObject top;
	public GameObject bottom;
	public GameObject left;
	public GameObject right;
	public GameObject bottomY; 
	public GameObject topY; 
	public GameObject view1; 
	public GameObject plungerView; 

	public GameObject menuball; //Where the camera should be when the menu is showing
	GameObject originalTrans;
	GameObject saveCameraPos; 
	GameObject ball=null; 
	GameObject[] balls;
	List<Transform> cameraPos; 

//	float directionTime = 0f;
//	Vector3 lastBallDirection = Vector3.zero;
	Vector3 pos; 
	Vector3 vpBall; 
	Vector3 vpBottom;

	float lastBallViewportY = 0f;
	float lastLeftViewpointX = 0f; 
	float timeInterval; 
	float timeFactor=1f; 
	float lastTime=0f;  
	float frames=0f; 
	float calculatedTime=0f;
	const float topThreshold = .5f; //How high should the ball be before we'll follow it?

	Animator animator; 
	Animation animation; 



	public enum State
	{
		//MenuShowing,
		View1_ZoomedOut,
		View2_ZoomedIn, 
		View3_Panning, 
		View4_BallLaunch
	};
	public State currentState;
	public State lastState; 

	// Use this for initialization
	void Start () {
		originalTrans = new GameObject (); 
		saveCameraPos = new GameObject (); 

		originalTrans.transform.position = view1.transform.position;
		originalTrans.transform.rotation = view1.transform.rotation;

	
	}

	void OnEnable()
	{
		animator = GetComponent<Animator> (); 
		animation = GetComponent<Animation> (); 
		animator.enabled = false;
		balls = new GameObject[]{gameFlow.ball, gameFlow.ball2, gameFlow.ball3}; 

		cameraPos = new List<Transform> (); 


	}

	void Update()
	{
		lastTime = Time.time; 
		frames++; 
		calculatedTime = lastTime / frames; 


	}



	void FixedUpdate()
	{

		timeInterval = (Time.deltaTime + Time.smoothDeltaTime + calculatedTime) / 3 * timeFactor; 

		if (gameFlow.menu.gameObject.activeSelf)
		{
			//The menu is up.
			//LerpTransform (gameObject, menuball);
			return;
		}

		switch (currentState)
		{
		case State.View1_ZoomedOut:
			DoView1Update ();
			break;
		case State.View2_ZoomedIn:
			DoView2Update();
			break;
		case State.View3_Panning:
			DoView3Update(); 
			break;
		case State.View4_BallLaunch:
			DoView4Update(); 
			break; 
		};
	}

	void DoView1Update()
	{

		LerpTransform (gameObject, originalTrans, 2f);

	}




	void DoView2Update () 
	{
		pos = transform.position; 
		
		if (BallsActive () > 1) {
			DoView1Update (); //Fallback in the case of multiple balls.
		} 
		else if (gameFlow.launcher.isTouchingLauncher)
		{
			DoView4Update(); 
		}
		else
		{
			
			SetView2Rotation(2f); 
			
			if (ball != null)
			{
				SetView2YPos(1f); 
				
				//If we can, show the flippers.
				vpBottom = GetComponent<Camera>().WorldToViewportPoint(bottom.transform.position);
				vpBall = GetComponent<Camera>().WorldToViewportPoint (ball.transform.position);
				
				
				lastBallViewportY = Mathf.Lerp (lastBallViewportY, vpBall.y, timeInterval); //This seems to smooth out some rough edges.
				
				
				MoveCamera(1f); 
				
				SetCameraBounds(); 
				
				transform.position = pos;
			}
		}
	}
	//float time=0f; 

	void DoView3Update()
	{

	}

	void DoView4Update()
	{
		LerpTransform (gameObject, plungerView, 2f); 
	}



	int BallsActive()
	{
		int numBallsActive = 0;
		 
		for(int i=0; i<balls.Length; i++)
		{
			if(balls[i].activeSelf)
			{
				ball = balls[i]; 
				numBallsActive++; 
			}
		}
		return numBallsActive; 
	}

	void SetView2Rotation(float speed)
	{
		transform.rotation = Quaternion.Lerp (transform.rotation, originalTrans.transform.rotation, timeInterval*speed);
	}


	void SetView2YPos(float speed)
	{
		float yDist = 8; 

		pos.x = Mathf.Lerp (pos.x, ball.transform.position.x, timeInterval*speed); 
		pos.y = Mathf.Lerp (pos.y, ball.transform.position.y+yDist, timeInterval*speed); 
	
	}

	Vector3 GetBallVelocity()
	{
		Vector3 ballVelocity = ball.GetComponent<Rigidbody>().velocity;
		ballVelocity.x = ballVelocity.y = 0f; //We're only worried about the z (up and down).
		ballVelocity.Normalize();
		return ballVelocity; 
	}

	void MoveCamera(float speed)
	{
		if (vpBall.y >= topThreshold)
		{
			//If vpBall is 1, lerp only toward the ball. If vpBall is topThreshold, lerp only toward the flippers.
			//Move down, toward the flippers, as much as possible.
			pos.z = Mathf.Lerp (transform.position.z, Mathf.Lerp (bottom.transform.position.z, ball.transform.position.z, (vpBall.y-topThreshold)*10f),timeInterval*speed);
		}
		
		else
		{
			pos.z = Mathf.Lerp (pos.z, bottom.transform.position.z, timeInterval*speed);
		}
	}

	void SetCameraBounds()
	{		 

		if (pos.z <= top.transform.position.z) 
		{
			pos.z = top.transform.position.z; 
		}
		if(pos.z >= bottom.transform.position.z)
		{
			pos.z = bottom.transform.position.z; 
		}
		if(pos.x >= left.transform.position.x)
		{
			pos.x = left.transform.position.x; 
		}
		if(pos.x <= right.transform.position.x)
		{
			pos.x = right.transform.position.x; 
		}
	}
	
	
	public void ToggleViews()
	{
		if (currentState == State.View1_ZoomedOut) {
			currentState = State.View2_ZoomedIn;
			Settings.instance.defaultView = State.View2_ZoomedIn; 
			Settings.instance.Save(); 
		} else if (currentState == State.View2_ZoomedIn) {
			currentState = State.View1_ZoomedOut;
			Settings.instance.defaultView = State.View1_ZoomedOut;
			Settings.instance.Save(); 
		}

	}

	public void DoPauseView()
	{
		if(currentState==State.View2_ZoomedIn)
		{
			saveCameraPos = gameObject; 
		}

		animator.enabled = true; 

	}

	public IEnumerator DoResumeView()
	{
		animator.enabled = false;
		currentState = Settings.instance.defaultView; 

		yield return null; 

	}

	public void LerpTransform(GameObject o1, GameObject o2, float speed)
	{
		o1.transform.position = Vector3.Lerp (o1.transform.position, o2.transform.position, timeInterval*speed);
		o1.transform.rotation = Quaternion.Lerp (o1.transform.rotation, o2.transform.rotation, timeInterval*speed);
	}

	public IEnumerator CreateTransform()
	{
		int transformPoints=20;  
		if (cameraPos.Count < transformPoints) {
			cameraPos.Add (this.transform); 
		}
		yield return new WaitForSeconds (5f); 
		StartCoroutine (CreateTransform ());  
		Debug.Log (cameraPos [0] + "," + animation["MoveCameraDuringPause"].time 
		           ); 

	}


}
