﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TextMeshPool : MonoBehaviour
{
	public static TextMeshPool textMeshPool; 
	public TextMesh pooledObject;
	public int pooledAmount = 20;
	public bool willGrow = true;
	public List<TextMesh> pooledObjects;

	void Awake()
	{
		textMeshPool = this; 
	}

	void Start ()
	{
		pooledObjects = new List<TextMesh>();
		for(int i = 0; i < pooledAmount; i++)
		{
			TextMesh obj = Instantiate(pooledObject) as TextMesh;
			obj.gameObject.SetActive(false);
			pooledObjects.Add(obj);
		}
	}

	public TextMesh GetPooledObject()
	{
		for(int i = 0; i< pooledObjects.Count; i++)
		{
			if(pooledObjects[i] == null)
			{
				TextMesh obj = Instantiate(pooledObject) as TextMesh;
				obj.gameObject.SetActive(false);
				pooledObjects[i] = obj;
				return pooledObjects[i];
			}
			if(!pooledObjects[i].gameObject.activeInHierarchy)
			{
				return pooledObjects[i];
			}
		}
		if (willGrow)
		{
			TextMesh obj = Instantiate(pooledObject) as TextMesh;
			pooledObjects.Add(obj);
			return obj;
		}
		return null;
	}
}