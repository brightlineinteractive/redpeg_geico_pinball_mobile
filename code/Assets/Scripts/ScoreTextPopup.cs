﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

public class ScoreTextPopup : MonoBehaviour {
	
	public GameFlow gameFlow; 

	TextMesh scoreText; 

	float fadeTime=0.5f; 
	
	public Sprite doubleUp; 
	public Sprite multiply; 
	public Sprite multiball; 

	public SpriteRenderer circleSprite;
	public GameObject centerGecko; 

	Animator circleAnimator; 
	AnimatorStateInfo currentState; 

	int anim=0; 

	Vector3 originalScale = new Vector3(1,1,1);
	Vector3 startScale = new Vector3(0,0,0); 
	Vector3 punchScale = new Vector3(2f, 2f, 2f); 
	Vector3 geckoOriginalScale; 

	Color originalColor; 

	// Use this for initialization
	void Start () {

		gameFlow = GameObject.Find ("GameFlow").GetComponent<GameFlow> (); 

		circleAnimator = circleSprite.gameObject.GetComponent<Animator> (); 
		circleAnimator.enabled = false; 
		anim = Animator.StringToHash ("Base Layer.CenterCircle"); 
		currentState = circleAnimator.GetCurrentAnimatorStateInfo (0); 
		circleSprite.sprite = null; 
		circleSprite.transform.localScale = startScale; 
		originalColor = circleSprite.color; 
		centerGecko.SetActive (true); 
		geckoOriginalScale = centerGecko.transform.localScale; 
	}

	public void DoubleScoreText(GameObject scoreObj)
	{
		circleSprite.sprite = doubleUp; 
		AddScoreText (0, scoreObj); 
		StartCoroutine (DoAnimation (circleSprite.gameObject)); 
	}

	public void MultiplyText()
	{
		circleSprite.sprite = multiply; 
		StartCoroutine (DoAnimation (circleSprite.gameObject)); 
	}

	public void MultiballText()
	{
		circleSprite.sprite = multiball; 
		StartCoroutine (DoAnimation (circleSprite.gameObject)); 
	}

	public void AddScoreText(int value, GameObject scoreObj)
	{
		scoreText = TextMeshPool.textMeshPool.GetPooledObject (); 
		if (scoreText == null)
			return; 
		if (value > 0) {
			scoreText.text = gameFlow.AddScore ((ulong)value).ToString ();
		} else {
			scoreText.text = "x2"; 
		}
		scoreText.transform.position = scoreObj.transform.position; 
		scoreText.gameObject.SetActive (true); 
		StartCoroutine (HideScoresAfterFade (scoreText)); 

	}
	


	IEnumerator HideScoresAfterFade(TextMesh scoreText)
	{
		float moveVal = 1f; 
		iTween.MoveBy(scoreText.gameObject,iTween.Hash("y" , moveVal, "time", fadeTime/2));		
		yield return new WaitForSeconds(fadeTime); 
		scoreText.gameObject.SetActive (false); 

	}

	IEnumerator DoAnimation(GameObject obj)
	{
		iTween.ScaleTo (centerGecko, iTween.Hash ("scale", startScale, "time", fadeTime)); 
		yield return new WaitForSeconds (fadeTime); 
		iTween.ScaleTo (obj, iTween.Hash ("scale", originalScale, "time", fadeTime)); 
		//obj.transform.localScale = originalScale; 
		yield return new WaitForSeconds (fadeTime*2); 
		 
		iTween.ScaleTo (obj, iTween.Hash ("scale", startScale, "time", fadeTime)); 
		yield return new WaitForSeconds (fadeTime*2); 
		//circleAnimator.enabled = false; 
		//centerGecko.SetActive (true); 
		circleSprite.sprite = null; 
		iTween.ScaleTo (centerGecko, iTween.Hash ("scale", geckoOriginalScale, "time", fadeTime)); 
		circleSprite.transform.localScale = startScale; 

	}

	/*void DoneScalingUp(GameObject obj)
	{
	}*/

	IEnumerator WaitForAnimation(AnimatorStateInfo anim)
	{
		Debug.Log (anim.length); 
		yield return new WaitForSeconds (anim.length); 
	}


}
