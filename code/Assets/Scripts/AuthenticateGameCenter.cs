﻿using UnityEngine;
using System.Collections;
using Prime31; 
using System;

public class AuthenticateGameCenter : MonoBehaviour {

	// Use this for initialization
	void Awake () {
		DontDestroyOnLoad (this.gameObject); 
		GameCenterAuthentication (); 
	}
	
	void GameCenterAuthentication()
	{
		#if UNITY_IOS
		GameCenterBinding.authenticateLocalPlayer ();
		#elif UNITY_ANDROID
		GPGManager.authenticationSucceededEvent += gpgAuthSuccess;
		GPGManager.authenticationFailedEvent += gpgAuthFailure;
		PlayGameServices.attemptSilentAuthentication();
		#endif
	
	}

	#if UNITY_ANDROID
	void gpgAuthSuccess(string userId)
	{
		Debug.Log("*****SIGNED IN to Google Play Services with userId=" + userId);
		PlayGameServices.setWelcomeBackToastSettings (GPGToastPlacement.Top, 0); 
	}
	
	bool retry = false; //the two-tries auth code created weird results, and will be removed if it passes testing without it.
	//after testing, I want to keep it in and disabled, the 'weird results' weren't due to this.
	
	void gpgAuthFailure(string errorMessage)
	{
		Debug.LogError("COULD NOT AUTHENTICATE: " + errorMessage);
		if (retry)
		{
			retry = false;
			PlayGameServices.authenticate();
		}
		else if (errorMessage.ToUpperInvariant().Contains("SERVICE_VERSION_UPDATE_REQUIRED"))
		{
			//retry = true; 
			showGpsUpdateDialog();
		}
	}
	
	
	void showGpsUpdateDialog()
	{
		try
		{
			//we must call the native java method GoogleApiAvailability.getErrorDialog(Activity, errCode),
			//which will require a bit of the ol' Reflection via AndroidJava* classes
			AndroidJavaClass unityPlayerJava, gmsJava;
			AndroidJavaObject activityJO = null, dialogJO = null;
			
			//Get the unity player class and extract the Activity object
			unityPlayerJava = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			activityJO = unityPlayerJava.GetStatic<AndroidJavaObject>("currentActivity");
			
			//We need to show dialog on the ui thread, like this:
			//activity.runOnUiThread(new Runnable() {
			//   public void run() { dialog.show(); }
			//});
			AndroidJavaRunnable showDialogOnUi = delegate()
			{
				try
				{
					gmsJava = new AndroidJavaClass("com.google.android.gms.common.GoogleApiAvailability");
					dialogJO = gmsJava.CallStatic<AndroidJavaObject>("getErrorDialog", activityJO, 2, 0);
				}
				catch
				{
					gmsJava = new AndroidJavaClass("com.google.android.gms.common.GooglePlayServicesUtil");
					dialogJO = gmsJava.CallStatic<AndroidJavaObject>("getErrorDialog", 2, activityJO, 0);
				}
				dialogJO.Call("show");
			};
			activityJO.Call("runOnUiThread", showDialogOnUi);
		}
		catch (Exception ex)
		{
			Debug.LogException(ex);
		}
	}
	
	
	#endif
}
