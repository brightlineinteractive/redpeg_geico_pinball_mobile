﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml.Serialization;

[System.Serializable]
public class Settings {
	
	public string leaderboardIP = "192.168.1.1";
	public CameraControl.State defaultView = CameraControl.State.View2_ZoomedIn;
	public float musicVolume = 1f;
	public float soundVolume = 1f;
	public Menu.GameType gameType = Menu.GameType.Ball3;
	public float rateQuoteHits = 0f; 
	public string firstInitial, middleInitial, lastInitial; 

	static public Settings instance = null;

	static public Settings Load()
	{
		string path = Application.persistentDataPath + "/settings.xml";
		if (File.Exists(path))
		{
			try {
				XmlSerializer s = new XmlSerializer(typeof(Settings));
				instance = (Settings)s.Deserialize(File.OpenRead(path));
			}
			catch (System.Exception ex)
			{
				Debug.Log("Settings error: " + ex.Message + Application.persistentDataPath);
			}
		}
		if (instance == null)
			instance = new Settings ();

		return instance;
	}

	public void Save()
	{
		XmlSerializer s = new XmlSerializer(typeof(Settings));
		StreamWriter path = new StreamWriter(Application.persistentDataPath + "/settings.xml");
		//s.Serialize (File.OpenWrite (path), instance);
		s.Serialize(path, instance);
		Debug.Log("Settings saved!");
		path.Close (); 
	}
}
