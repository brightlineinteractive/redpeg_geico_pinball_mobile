﻿using UnityEngine;
using System.Collections;

public class BallLauncher : MonoBehaviour {

	//variable to hold if the LaunchBall command button has been pressed
	private bool launchButtonPressed = false;
	//variable to hold if the ball is touching the ball launcher
	public bool isTouchingLauncher = false;
	public AudioClip flipperKick;
	public GameObject ball;

	public float maxSpeed = 50f;

	public Rigidbody launcherBody;
	Vector3 basePosition;
	public float lastPlungerValue = 0f;

	public bool isEnabled=false; 

	void Awake()
	{
		basePosition=launcherBody.position;
	}

	void Update ()
	{
		if (isEnabled) {
			//check if the LaunchBall command button is pressed and set the variable acordingly
			if (Input.GetButton ("LaunchBall")) {
				launchButtonPressed = true;
			} else if (isTouchingLauncher && !float.IsNaN (InputHelper.plunger)) {
				//Held down
				launcherBody.MovePosition (new Vector3 (basePosition.x, basePosition.y, basePosition.z + InputHelper.plunger));
				launchButtonPressed = true;
				lastPlungerValue = InputHelper.plunger;
			} else if (isTouchingLauncher && float.IsNaN (InputHelper.plunger) && !float.IsNaN (lastPlungerValue)) {
				//Released
				launcherBody.MovePosition (new Vector3 (basePosition.x, basePosition.y, basePosition.z));
				float velocity = -lastPlungerValue * maxSpeed;
				if (!ball.GetComponent<Rigidbody>().isKinematic) {
					ball.GetComponent<Rigidbody>().velocity = new Vector3 (0, 0, velocity);
				}
				lastPlungerValue = float.NaN;
			} else {
				launchButtonPressed = false;
			}

		}

	}
	
	
	/*void FixedUpdate ()
	{
		//check if the LaunchBall command button is pressed and if the ball is touching the ball launcher, launch the ball!
//		if(launchButtonPressed)
//		{
//			if(isTouchingLauncher)
//			{
//				if(ball.rigidbody)
//				{
//					ball.rigidbody.velocity =new Vector3(0,0,-50);
//				}
//			}	
//		}	
	}*/
	
	void OnTriggerEnter (Collider other) 
	{
		//set the variable to true when the ball starts touching the ball launcher
		if(other.gameObject.CompareTag("Ball"))
		{
			ball = other.gameObject; 
			isTouchingLauncher = true;
		}
	}
	
	void OnTriggerExit (Collider other) 
	{
		//set the variable to false when the ball is not touching the launcher anymore
		if(other.gameObject.CompareTag("Ball"))
		{
			isTouchingLauncher = false;	
		}
	}
}
