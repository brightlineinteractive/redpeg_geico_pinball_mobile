﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InitialsInput : MonoBehaviour {

	[Header("Logic Processors")]
	public GameFlow gameFlow;

	[Header("UI")]
	public Image blackBackground;
	public Text[] letterArr = new Text[3];
	private int selectedLabelIndex = 0;
	private Text selectedLabel;
	
	private string[] alphabet = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
	private int[] letterIndex;

	private string endName;

	private float cursorFlashTime = 0.5f;
	private float currentFlashTimer = 0f;

	private bool cursorOn = false;

	float timeout = 30f;
	float openTime = 0f;

	// Use this for initialization
	void Awake () 
	{
		letterIndex = new int[letterArr.Length];

		gameObject.SetActive (false);
	}

	public void SetTimeout(float time)
	{
		timeout = time;
	}

	public void SetBackOpacity(float alpha)
	{
		blackBackground.color = new Color(blackBackground.color.r,blackBackground.color.g,blackBackground.color.b,alpha);
	}

	void OnEnable()
	{
		selectedLabelIndex = 0;
		selectedLabel = letterArr [0];
		for (int i = 0;i < letterArr.Length;i++)
		{
			letterArr[i].text = "A";
			letterIndex[i] = 0;
		}
		endName = "";
		openTime = Time.time;
	}

	// Update is called once per frame
	void Update ()
	{
		while (openTime > 0f) //Advance the selection until we finish and close.
			AdvanceSelection ();

		if (openTime != 0f && Time.time - openTime >= timeout) {
			while (openTime > 0f) //Advance the selection until we finish and close.
				AdvanceSelection ();
		}

		if (Input.GetButtonDown("LeftFlipper"))
		{
			PreviousLetter();
		}
		else if (Input.GetButtonDown("RightFlipper"))
		{
			AdvanceLetter();
		}

		if (selectedLabel != null)
		{
			currentFlashTimer += Time.deltaTime;

			if (currentFlashTimer > cursorFlashTime)
			{
				SwitchCursor();
			}
		}

		if (Input.GetKeyDown(KeyCode.S))
		{
			AdvanceSelection();
		}
	}

	private void SwitchCursor()
	{
		if (cursorOn)
		{
			selectedLabel.text = "_";
		}
		else
		{
			selectedLabel.text = alphabet [letterIndex[selectedLabelIndex]];
		}

		currentFlashTimer = 0f;
		cursorOn = !cursorOn;
	}

	private void AdvanceLetter()
	{
		letterIndex [selectedLabelIndex]++;
		if (letterIndex[selectedLabelIndex] > alphabet.Length - 1)
		{
			letterIndex[selectedLabelIndex] = 0;
		}
		selectedLabel.text = alphabet [letterIndex[selectedLabelIndex]];

		currentFlashTimer = 0f;
	}

	private void PreviousLetter()
	{
		letterIndex [selectedLabelIndex]--;
		if (letterIndex[selectedLabelIndex] < 0)
		{
			letterIndex[selectedLabelIndex] = alphabet.Length - 1;
		}
		selectedLabel.text = alphabet [letterIndex[selectedLabelIndex]];

		currentFlashTimer = 0f;
	}

	private void AdvanceSelection()
	{
		selectedLabel.text = alphabet [letterIndex[selectedLabelIndex]];

		selectedLabelIndex++;

		if (selectedLabelIndex > letterArr.Length - 1)
		{
			End();
			return;
		}

		selectedLabel = letterArr [selectedLabelIndex];

		currentFlashTimer = 0f;
	}

	private void End()
	{
		openTime = 0f;
		for (int i = 0;i < letterArr.Length;i++)
		{
			endName = endName + letterArr[i].text;
		}
		Debug.Log ("InitialsInput:: End() : End Name: " + endName);
//		gameFlow.SendInfoToBus (endName);
		//background.SetActive (false);
	}
}
