﻿using UnityEngine;
using System.Collections;
using System; 

public class ChuteHandler : MonoBehaviour {
    public float ExitLaunchSpeed = 1;

	public float trapSeconds=1f;
	private float trapTime=-1f;
	private float trapStartTime=-1f;
	public float trailSeconds=0.5f;
	private GameObject trappedBall;
	public GameObject trapHandler;
	private TrapHandler trapHandlerComponent;
	public AudioClip audioClip;
	private GameFlow gameFlow; 


	bool inChute=false; 
	bool isLaunching=false; 

	void Awake()
	{
		DontDestroyOnLoad (this.gameObject); 
	}

	void Start () {

		if(trapHandler){
			trapHandlerComponent=trapHandler.GetComponent<TrapHandler>();
		}

		StartCoroutine (CheckBallInChute ());

        if ((ExitLaunchSpeed = Mathf.Abs(ExitLaunchSpeed)) == 0)
            throw new UnityException("ExitLaunchSpeed isn't set, so balls wont exit chutes!");

		gameFlow = GameObject.FindObjectOfType<GameFlow> (); 
	}

	void Update()
	{

	}

	IEnumerator CheckBallInChute()
	{
		if (trappedBall != null) {
			if (gameObject.GetComponent<Collider>().bounds.Contains (trappedBall.transform.position)) {
				trapHandlerComponent.OpenDoors(); 
				StartCoroutine (LaunchBall (trappedBall)); 
			}
		}
		yield return new WaitForSeconds (trapSeconds); 
		StartCoroutine (CheckBallInChute ()); 
	}
	
	void OnTriggerEnter(Collider other){
		if (other.CompareTag ("Ball")) {
			inChute=true;
			if(!isLaunching){
				StartCoroutine (LaunchBall(other.gameObject)); 
			}
		} 
		ToggleDoors (); 
	}

	void OnTriggerExit(Collider other)
	{
		if(other.CompareTag("Ball"))
		{
			inChute = false; 
		}
		trapHandlerComponent.CloseDoors (); 
		StartCoroutine(PlayAudio ()); 

	}
	
	void ToggleDoors()
	{
		if(!inChute)
		trapHandlerComponent.CloseDoors ();
		else{
			trapHandlerComponent.OpenDoors(); 
		}
	}

	

	private IEnumerator LaunchBall(GameObject ball)
	{
		trapHandlerComponent.OpenDoors (); 
		isLaunching = true; 
		trapStartTime=Time.realtimeSinceStartup;
		trappedBall = ball;
		yield return new WaitForSeconds(trapSeconds);
		if(trappedBall&&!gameFlow.isPaused){
			trappedBall.GetComponent<Rigidbody>().velocity =new Vector3(0,0,-ExitLaunchSpeed);
		}
		trapStartTime=-1;
		trapTime=-1;
		trappedBall.transform.FindChild("Trail").gameObject.SetActive(true);
		yield return new WaitForSeconds(trailSeconds);
		trappedBall.transform.FindChild("Trail").gameObject.SetActive(false);
		isLaunching = false; 
	}

	bool isPlaying=false; 

	IEnumerator PlayAudio()
	{
		if(audioClip.isReadyToPlay&&!isPlaying) {
			isPlaying = true; 
			AudioSource.PlayClipAtPoint(audioClip,transform.position, Settings.instance.soundVolume );	
		}
		yield return new WaitForSeconds (audioClip.length); 
		isPlaying = false; 
	}


}
