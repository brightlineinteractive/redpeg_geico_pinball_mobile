﻿using UnityEngine;
using System.Collections;

public class LightGroupHandler : MonoBehaviour {

	public GameObject[] lightObjects;
	private GameFlow gameFlowGC;
	public AudioSource sound;
	
	void Start () {
		if (gameFlowGC == null)
			gameFlowGC=GameObject.Find("GameFlow").GetComponent<GameFlow>();
	}

	public void CheckLights(){
		StartCoroutine (CheckLightsCo ());
	}

	private IEnumerator CheckLightsCo()
	{
		yield return new WaitForSeconds(0.1f);
		bool allActive=true;
		for(int i = 0; i<lightObjects.Length; i++){
			if(!lightObjects[i].activeSelf){
				allActive=false;
			}
		}
		if(allActive){
			gameFlowGC.IncreaseMultiplier();
			if(!sound.isPlaying) {
				sound.PlayOneShot(sound.clip, Settings.instance.soundVolume); 	
			}
			for(int j = 0; j<lightObjects.Length; j++){
				lightObjects[j].SetActive(false);
				yield return new WaitForSeconds(0.1f);
				
			}
		}
	}
}
