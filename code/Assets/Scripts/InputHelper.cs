﻿using UnityEngine;
using System.Collections;
using System; 

public class InputHelper : MonoBehaviour {

	public GameObject flipperMid;
	public GameObject plungerLeft;
	public Camera mainCamera;
	public BallLauncher launcher;

	public float plungerMultiplier = 1.6f;

	public delegate bool InputCheckFunction(Vector2 point);

	static InputHelper instance;

	static public bool leftFlipper = false;
	static public bool rightFlipper = false;
	static public float plunger = 0f;
	static public bool start;
	static public GameObject touchBall; 
	static public bool isEnabled=true; 

	float plungerInitialTouch;

	int ballLayer; 
	//int ballMask;

	public LayerMask ballMask; 

	void Awake()
	{
		instance = this;
		//ballLayer =  LayerMask.NameToLayer ("Ball"); 
		//ballMask = 1 << ballLayer; 
	}

	void LateUpdate()
	{
		if (isEnabled) {
			bool leftTemp = false, rightTemp = false, startTemp = false;
			float plungerTemp = float.NaN;
			Vector2 flipperCoord = mainCamera.WorldToViewportPoint (flipperMid.transform.position);
			Vector2 plungerLeftCoord = mainCamera.WorldToViewportPoint (plungerLeft.transform.position);
			PerTouch ((p) =>
			{
				if (launcher.isTouchingLauncher && p.x > plungerLeftCoord.x) {
					if (float.IsNaN (plunger)) {
						plungerInitialTouch = p.y;
						plungerTemp = 0f;
					} else {
						plungerTemp = Mathf.Clamp ((plungerInitialTouch - p.y) * plungerMultiplier, 0f, 1f);
					}
				} else if (p.x < flipperCoord.x)
					leftTemp = true;
				else if (p.x > flipperCoord.x)
					rightTemp = true;

				if (p.x >= .2f && p.x <= .8f &&
					p.y >= .2f && p.y <= .8f) {
					startTemp = true;
				}
			});
			plunger = plungerTemp;
			leftFlipper = leftTemp;
			rightFlipper = rightTemp;
			plunger = plungerTemp;
			start = startTemp;
			touchBall = IsTouchingBall (); 
		}
	}

	static Vector2 point = new Vector2();
	static public bool IsTouching(InputCheckFunction func)
	{
		if (Input.GetMouseButton(0)) 
		{
			point.x = Input.mousePosition.x / Screen.width;
			point.y = Input.mousePosition.y / Screen.height;
			if (func(point)) return true;
		}
		
		for (int i = 0; i < Input.touches.Length; i++)
		{
			point.x = Input.touches[i].position.x/Screen.width;
			point.y = Input.touches[i].position.y/Screen.height;
			if (func(point)) return true;
		}
		return false;
	}

	static public void PerTouch(System.Action<Vector2> func)
	{
		if (Input.GetMouseButton(0)) 
		{
			point.x = Input.mousePosition.x / Screen.width;
			point.y = Input.mousePosition.y / Screen.height;
			func(point);
		}
		
		for (int i = 0; i < Input.touches.Length; i++)
		{
			point.x = Input.touches[i].position.x/Screen.width;
			point.y = Input.touches[i].position.y/Screen.height;
			func(point);
		}
	}

	static public int GetNumTouches()
	{
		return Input.GetMouseButton (0) ? Input.touchCount + 1 : Input.touchCount;
	}

	GameObject IsTouchingBall()
	{
		try{
			if (Input.touchCount > 0) {
				Ray ray = mainCamera.ScreenPointToRay (Input.GetTouch (0).position);
				RaycastHit rayHit; 
				if (Physics.Raycast (ray, out rayHit, 20f, ballMask.value)) {
					if (rayHit.collider.gameObject.tag == "Ball") {
						return rayHit.collider.gameObject; 
					} else {
						return null; 
					}
				} else {
					return rayHit.collider.gameObject;  
				}
			}
			else{
				return null; 
			}
		}
		catch(NullReferenceException e)
		{
			//Debug.LogException (e); 
			return null; 
		}
	}
	
}
