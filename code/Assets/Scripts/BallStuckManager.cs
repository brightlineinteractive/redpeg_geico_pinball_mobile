using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

public class BallStuckManager : MonoBehaviour {

	public LeftFlipper leftFlipper; 
	public RightFlipper rightFlipper; 
	public BallLauncher ballLauncher; 

	public GameFlow gameFlow; 

	public float stuckSpeed=.5f; 

	public float stuckTimeout=20f; 
	public float timingOutValue; 

	public List<GameObject> stuckBalls = new List<GameObject> (); 


	// Use this for initialization
	void Start () {
		timingOutValue = stuckTimeout; 
	}

	void Update()
	{
		for (int i=0; i<gameFlow.balls.Length; i++) {
			if (gameFlow.balls [i].activeSelf) {
				if (!ballLauncher.isTouchingLauncher && !leftFlipper.button && !rightFlipper.button && !gameFlow.isPaused) 
				{
					if (gameFlow.balls [i].GetComponent<Rigidbody>().velocity.magnitude < stuckSpeed) {
						if(!stuckBalls.Contains(gameFlow.balls[i]))
							stuckBalls.Add (gameFlow.balls[i]); 
					}
					else{
						if(stuckBalls.Contains(gameFlow.balls[i]))
							stuckBalls.Remove(gameFlow.balls[i]); 
					}
					
					
				}
				
			}
		}

		for (int j=0; j<stuckBalls.Count; j++) 
		{
			if(stuckBalls[j].GetComponent<Rigidbody>().velocity.magnitude < stuckSpeed){
				if ((timingOutValue -= Time.deltaTime) <= 0){
					timingOutValue = stuckTimeout; 
					stuckBalls[j].gameObject.SetActive(false); 
					gameFlow.InitializeBall(stuckBalls[j], gameFlow.BallStartPos); 
					stuckBalls.RemoveAt(j); 

					
				
				}
			}

			else{
				timingOutValue = stuckTimeout; 
			}

		}

		if(gameFlow.isPaused||leftFlipper.isTouchingBall||rightFlipper.isTouchingBall||ballLauncher.isTouchingLauncher)
		{
			timingOutValue = stuckTimeout; 
		}


	
		
	}
	
	
}
