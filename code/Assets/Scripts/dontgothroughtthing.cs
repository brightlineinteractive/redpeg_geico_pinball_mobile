﻿using UnityEngine;
using System.Collections;

public class dontgothroughtthing : MonoBehaviour {

	public LayerMask layerMask; //make sure we aren't in this layer 
	public float skinWidth= 0.1f; //probably doesn't need to be changed 
	private float minimumExtent; 
	private float partialExtent; 
	private float sqrMinimumExtent; 
	private Vector3 previousPosition; 
	private Rigidbody myRigidbody; 

	RaycastHit hitInfo;

	//initialize values 
	void Awake() { 
		myRigidbody = GetComponent<Rigidbody>(); 
		previousPosition = myRigidbody.position; 
		minimumExtent = Mathf.Min(Mathf.Min(GetComponent<Collider>().bounds.extents.x, GetComponent<Collider>().bounds.extents.y), GetComponent<Collider>().bounds.extents.z); 
		partialExtent = minimumExtent*(1.0f - skinWidth); 
		sqrMinimumExtent = minimumExtent*minimumExtent; 
		hitInfo = new RaycastHit(); 
	} 
	
	public void Reset(){
		myRigidbody = GetComponent<Rigidbody>(); 
		previousPosition = myRigidbody.position; 
		minimumExtent = Mathf.Min(Mathf.Min(GetComponent<Collider>().bounds.extents.x, GetComponent<Collider>().bounds.extents.y), GetComponent<Collider>().bounds.extents.z); 
		partialExtent = minimumExtent*(1.0f - skinWidth); 
		sqrMinimumExtent = minimumExtent*minimumExtent; 
	}
	
	void FixedUpdate() { 
		//have we moved more than our minimum extent? 
		if ((previousPosition - myRigidbody.position).sqrMagnitude > sqrMinimumExtent) { 
			Vector3 movementThisStep = myRigidbody.position - previousPosition; 
			float movementMagnitude = movementThisStep.magnitude; 
			//check for obstructions we might have missed 
			if (Physics.Raycast(previousPosition, movementThisStep,out hitInfo, movementMagnitude, layerMask.value)) 
				myRigidbody.position = hitInfo.point - (movementThisStep/movementMagnitude)*partialExtent; 
		} 
		previousPosition = myRigidbody.position; 
		
		
	}
}
