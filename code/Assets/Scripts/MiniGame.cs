﻿using UnityEngine;
using System.Collections;

public class MiniGame : MonoBehaviour {

	public float trapTime=1;
	public bool sinking=false;
	public GameObject leftFlipper;
	public GameObject rightFlipper;
	public bool deactivated=false;
	public float reactivationTime=1;
	private float lastTimeHit=-1;
	private float deactivationTime=-1;
	private GameFlow gameFlowComponent;
	private TrapHandler trapHandler;
	private GameObject trappedBall;
	private Vector3 startPos;
	public bool isMultiball; 

	public AudioClip sound1; 
	public AudioClip sound2; 
	public AudioSource alarm; 

	public dontgothroughtthing scriptVar;
	public ScoreTextPopup scoreTextPopup; 
	ScoreboardAnimationHandler scoreboard; 

	float originalAlarmVolume; 

	void Start () {
		gameFlowComponent=GameObject.Find("GameFlow").GetComponent<GameFlow>();
		trapHandler=GameObject.Find("TrapHandler").GetComponent<TrapHandler>();
		scoreboard = GameObject.Find ("ScoreboardAnimationHandler").GetComponent<ScoreboardAnimationHandler> (); 
		iTween.Init(gameObject);
		originalAlarmVolume = alarm.volume; 
	}
	void OnTriggerEnter(Collider other){
		alarm.volume = Settings.instance.soundVolume*originalAlarmVolume; 
		if(deactivated!=true){
			Debug.Log("trigger entered active");
			deactivated=true;
			trappedBall=other.gameObject;
			scriptVar=trappedBall.GetComponent<dontgothroughtthing>();
			scriptVar.enabled=false;
		iTween.MoveTo(trappedBall,iTween.Hash("position",transform.position,"time",trapTime,"oncomplete","TweenComplete","oncompletetarget",gameObject));
		} else {
			Debug.Log("trigger entered deactivated");
		}

		StartCoroutine (DoSound ()); 
	}
	
	void TweenComplete(){
		
		Debug.Log(trappedBall.name);
		Debug.Log("Tween Complete");

		StartCoroutine (BallActivationCo ());
	}

	IEnumerator DoSound()
	{
		alarm.Play ();
		AudioSource.PlayClipAtPoint(sound1, transform.position, Settings.instance.soundVolume);	
		yield return new WaitForSeconds (sound1.length); 
		AudioSource.PlayClipAtPoint (sound2, transform.position, Settings.instance.soundVolume); 
		yield return new WaitForSeconds (sound2.length); 
		alarm.Stop (); 

	}

	private IEnumerator BallActivationCo()
	{
		trappedBall.SetActive(false);
		
		sinking=false;
		gameFlowComponent.ActivateLightsForSeconds(1);
		Debug.Log("activate then wait");
		Debug.Log("Activate then wait again");
		gameFlowComponent.ActivateBall();
		yield return new WaitForSeconds (0.5f); 
		Debug.Log("Activate then wait again");
		gameFlowComponent.ActivateBall();
		yield return new WaitForSeconds(0.5f);
		//gameFlowComponent.DeactivateLights();
		yield return new WaitForSeconds(0.5f);
		deactivated=false;
		scriptVar.enabled=true;
		scriptVar.Reset();
		trappedBall=null;

		scoreTextPopup.MultiballText (); 
		StartCoroutine(scoreboard.Animate (scoreboard.multiBall)); 
	}
}
