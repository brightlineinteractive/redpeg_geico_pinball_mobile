﻿using UnityEngine;
using System.Collections;

public class TrapHandler : MonoBehaviour {
	
	public float transitionTime=1;
	private float startTime=-1;
	public GameObject leftTrap;
	public GameObject rightTrap;
	public float rightClosedRotationY=154;
	public float rightOpenRotationY=80;
	public float leftClosedRotationY=22;
	public float leftOpenRotationY=92;
	public bool open=true;
	private float currTime;

	public void OpenDoors(){
//		Debug.Log("opening trap doors");
		iTween.RotateTo(leftTrap,new Vector3(0,leftOpenRotationY,0),transitionTime);
		iTween.RotateTo(rightTrap,new Vector3(0,rightOpenRotationY,0),transitionTime);
	}
	public void CloseDoors(){
		Debug.Log("closing trap doors");
		iTween.RotateTo(leftTrap,new Vector3(0,leftClosedRotationY,0),transitionTime);
		iTween.RotateTo(rightTrap,new Vector3(0,rightClosedRotationY,0),transitionTime);
	}
}
