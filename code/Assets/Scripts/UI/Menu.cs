﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System; 
public class Menu : MonoBehaviour {

	public enum GameType
	{
		NONE,
		Time15,
		Time30,
		Ball3,
		Ball1,
		StressTest
	};
	public GameType gameType = GameType.StressTest;
	GameType prevGameType;

	GameFlow game;
	public GameObject backButton;
	public GameObject homeButton;
	public GameObject pauseButton; 
	public GameObject cameraButton; 

	public GameObject pauseMenu;
	public GameTypeMenu gameTypeMenu;
	public SettingsMenu settingsMenu;
	public GameObject gameOverMenu;
	public GameObject newGameMenu; 
	public GameObject howToPlay; 
	public GameObject leaderboard; 
	public GameObject about; 
	public GameObject lightBox; 
	public GameObject rateQuote; 

	public GameObject leaderboardEntry; 

	Stack<GameObject> menuStack;
	GameObject currentMenu = null;

	Text GameTypeText;

	[System.Serializable]
	public class GameTypeSprite
	{
		public Sprite hitState; 
		public Sprite defState; 
	}

	public GameTypeSprite time15Sprites; 
	public GameTypeSprite time30Sprites; 
	public GameTypeSprite ball1Sprites; 
	public GameTypeSprite ball3Sprites; 

	Button gameTypeButton; 

	public GameObject logo; 

	RectTransform defaultBackPos; 
//	RectTransform defaultPlayPos; 

	Vector2 defaultAnchorMin; 
	Vector2 defaultAnchorMax; 

	public CameraControl cameraControl; 
	public ScoreboardAnimationHandler scoreAnim; 


	// Use this for initialization
	public void Init (GameFlow g) {
		game = g;
		currentMenu = null;
		menuStack = new Stack<GameObject> ();
		gameTypeMenu.Init (this, game);
		settingsMenu.Init (game);
		SelectGameType (gameType);
		defaultBackPos = backButton.GetComponent<RectTransform> (); 
//		defaultPlayPos = homeButton.GetComponent<RectTransform> ();

		defaultAnchorMin = defaultBackPos.anchorMin; 
		defaultAnchorMax = defaultBackPos.anchorMax; 

	}



	
	public bool IsShowing()
	{
		return gameObject.activeSelf;
	}

	//pause: Is the base menu the New Game or the Resume?
	//screen: Start on a menu besides the base.
	public void Show(bool pause = false, GameObject screen = null)
	{
		gameObject.SetActive (true);
		logo.SetActive (true); 
		lightBox.SetActive (true); 

		if (pause)
		{
			if(!game.inGame)
			{
				ShowMenu (newGameMenu); 
			}
			else{
				ShowMenu (pauseMenu);
			}

		}
		else
		{
			ShowMenu(newGameMenu);
		}

		if (screen != null)
		{

			ShowMenu (screen);
	
		}
		cameraControl.DoPauseView (); 
		scoreAnim.DoLogoAnimation (); 
		cameraButton.SetActive (false); 
		pauseButton.SetActive (false); 
	}

	public void Hide()
	{
		prevGameType = gameType;
		foreach (Transform child in transform) 
		{
			child.gameObject.SetActive(false); 
		}
		gameObject.SetActive (false);
		Home ();
		currentMenu = null;
		cameraControl.DoResumeView (); 
		scoreAnim.DisableAnimation (); 
		cameraButton.SetActive (true); 
		pauseButton.SetActive (true); 

		Resources.UnloadUnusedAssets (); 
		System.GC.Collect (); 
	}

	public void ShowMenu(GameObject menu)
	{

		if (currentMenu != null)
		{
			menuStack.Push (currentMenu);
			currentMenu.SetActive (false);

			menu.SetActive (true);
		}
		else
		{
			menu.SetActive (true);

			//Show the game type.
			//Transform gameTypeTransform = GameObject.Find ("GameType").GetComponent<Transform>(); 
			//gameTypeButton.GetComponent<Button>().enabled = false;
			//ShowGameTypeImage(); 
			//gameTypeTransform.FindChild("Text").GetComponent <Text>().text = "Game Type (" + GetGameTypeString(gameType) + ")";
		}



		currentMenu = menu; 

		AdjustBottomButton (backButton, true, false);
		AdjustBottomButton (homeButton, true, false);

		if (menu == gameTypeMenu.gameObject) 
		{
			prevGameType = gameType;
			SetBackAndNewGameActive(true, true); 
			
		}
		if(menu == howToPlay)
			SetBackAndNewGameActive(true, true);
		if(menu == settingsMenu.gameObject)
			SetBackAndNewGameActive(true, true);
		if (menu == leaderboard) 
			SetBackAndNewGameActive (true, true); 
		if(menu==about)
			SetBackAndNewGameActive(true, false); 
		if (menu == pauseMenu) {
			SetBackAndNewGameActive (false, false); 
			ShowGameTypeImage(); 
		}
		if (menu == newGameMenu) {
			SetBackAndNewGameActive (false, false); 
			ShowGameTypeImage(); 
		}



		SetScrollbarToTop (); 


	}

	void SetBackAndNewGameActive(bool back, bool newGame)
	{
		if (back)
			backButton.SetActive (true);
		else
			backButton.SetActive (false); 
		if (newGame)
			homeButton.SetActive (true);
		else 
			homeButton.SetActive (false); 

		if (back && !newGame) 
		{
			AdjustBottomButton (backButton, true, true); 
		}

	}

	void ShowGameTypeImage()
	{
		try{
			gameTypeButton = GameObject.Find ("GameType").GetComponent<Button> ();
		}
		catch(NullReferenceException e){
			return; 
		}

		if (gameTypeButton != null) {
			switch (gameType) {
			case GameType.Time15:
				SetGameTypeSprites (gameTypeButton, time15Sprites); 
				break;
			case GameType.Time30:
				SetGameTypeSprites (gameTypeButton, time30Sprites); 
				break;
			case GameType.Ball1:
				SetGameTypeSprites (gameTypeButton, ball1Sprites); 
				break; 
			case GameType.Ball3:
				SetGameTypeSprites (gameTypeButton, ball3Sprites); 
				break;
			}
		}

		gameTypeButton = null; 

	}

	void SetGameTypeSprites (Button button, GameTypeSprite sprites)
	{
		SpriteState spriteState = new SpriteState (); 

		spriteState.disabledSprite = sprites.defState; 
		spriteState.highlightedSprite = sprites.hitState;
		spriteState.pressedSprite = sprites.hitState; 

		button.spriteState = spriteState; 

		button.image.sprite = sprites.defState; 

	}

	public void Back()
	{
		SetBackAndNewGameActive (false, false); 
		if (menuStack.Count == 0)
			return;


		GameObject backMenu = menuStack.Pop ();
		backMenu.SetActive (true);
		currentMenu.SetActive (false);
		currentMenu = backMenu;

	//	AdjustBottomButton (backButton, true, false);
	//	AdjustBottomButton (homeButton, true, false);

		ShowGameTypeImage (); 

		//If we're coming back out of the GameType menu and we're in a game,
		//	revert the gameType.
		if (currentMenu == gameTypeMenu.gameObject && game.inGame)
		{
			SelectGameType(prevGameType);
			
		}
	/*	if (menuStack.Count == 0)
		{
			backButton.SetActive (false);
			homeButton.SetActive (false);
			//Show the game type.
		}*/

		//GameObject.Find("GameType").transform.FindChild("Text").GetComponent <Text>().text = "Game Type (" + GetGameTypeString() + ")";

	}

	public void Home()
	{
		while (menuStack.Count > 0)
			Back ();
	}

	//Game Type Menu
	//

	static public string GetGameTypeString(GameType gameType)
	{
		switch (gameType) {
		case GameType.NONE:
			return "All Games";
			break;
		case GameType.Time30:
			return "30 Sec";
			break;
		case GameType.Time15:
			return "15 Sec";
			break;
		case GameType.Ball3:
			return "3 Ball";
			break;
		case GameType.Ball1:
			return "1 Ball";
			break;
		case GameType.StressTest:
			return "Dev";
			break;
		default:
			return "Err";
			break;
		};
	}

	public void SelectGameType(GameType g)
	{
		gameType = g;
		switch (gameType) {
		case GameType.Time30:
			game.SetGameTypeTimed (30);
			break;
		case GameType.Time15:
			game.SetGameTypeTimed (15);
			break;
		case GameType.Ball3:
			game.SetGameTypeBalls (3);
			break;
		case GameType.Ball1:
			game.SetGameTypeBalls (1);
			break;
		case GameType.StressTest:
			game.SetGameTypeStressTest ();
			break;
		default:
			break;
		};
		gameTypeMenu.SetSelectedGameType (g);
		Debug.Log (g.ToString () + " selected.");
	}
	//

	void AdjustBottomButton(GameObject b, bool Y, bool X)
	{
		RectTransform menuTrans = currentMenu.GetComponent<RectTransform> ();
		RectTransform t = b.GetComponent<RectTransform> ();
		Vector2 temp; 
		if (Y) {
			temp = t.anchorMax; 
			temp.y = menuTrans.anchorMin.y - .01f;
			t.anchorMax = temp;
			temp = t.anchorMin;
			temp.y = t.anchorMax.y - .16f;
			t.anchorMin = temp;
		}
		/*	if(t.gameObject==backButton)
			{
				temp = t.anchorMin; 
				temp.x = defaultAnchorMin.x; 
				t.anchorMin = temp; 
				temp.x = defaultAnchorMax.x; 
				t.anchorMax = temp; 
			}
		} 
		 if (X) {
			temp = t.anchorMin;  
			temp.x = menuTrans.anchorMin.x; 
			t.anchorMin = temp; 
			temp.x = t.anchorMin.x + .13f; 
			t.anchorMax = temp; 
		} 
	/*	else
		{

		}*/

	}

	void SetScrollbarToTop()
	{
		Scrollbar scrollBar; 
		try{
			scrollBar = GameObject.FindObjectOfType<Scrollbar>(); 
			scrollBar.value = 1f; 
		}
		catch(NullReferenceException e)
		{
			return; 
		}
	}


}
