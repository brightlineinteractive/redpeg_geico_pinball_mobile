﻿using UnityEngine;
using UnityEngine.UI; 
using System.Collections;
using Prime31; 

public class LeaderboardEntry : MonoBehaviour {

	public Text firstInitial; 
	public Text middleInitial; 
	public Text lastInitial; 

	public Text placeText; 
	public Text scoreText; 

	public Image firstImage; 
	public Image middleImage; 
	public Image lastImage; 

	public Button doneButton;

    public Sprite normalInitialSprite;
    public Sprite selectedInitialSprite;

    public float selectedBlinkFreq = 1;

    public System.Action OnComplete;

	public GameFlow gameFlow; 


	Image[] initials; 

	//long playerScore; 
	string iOSLeaderboardId = @"GeicoPinballLeaderboard";
    float blinkTime = 0;

	private enum Selected
	{
		NONE,
		first,
		middle,
		last
	}; 

	Selected selected;  

	bool initialized=false; 
	bool buttonDown = false;


	#region monobehaviours

	// Use this for initialization
	void Start () {

		selected = Selected.NONE; 

		initials = new Image[]{firstImage, middleImage, lastImage}; 

		PopulateInitialsFromXml (); 

		OnFirstInitialPress (); 


	}


	
	// Update is called once per frame
    void Update()
    {
        if (InputHelper.GetNumTouches() == 0)
        {
            buttonDown = false;
        }

        ResetSelectedImage();
        if ((blinkTime -= Time.deltaTime) <= 0)
            blinkTime = selectedBlinkFreq;
        else if(blinkTime < selectedBlinkFreq / 2) //for each blink cycle, first half dont change sprite, 2nd half change it using switch below:
            switch (selected)
            {
                case Selected.first:
                    firstImage.sprite = selectedInitialSprite;
                    break;
                case Selected.middle:
                    middleImage.sprite = selectedInitialSprite;
                    break;
                case Selected.last:
                    lastImage.sprite = selectedInitialSprite;
                    break;
                case Selected.NONE:
                default:
                    break;
            }
    }
	#endregion 

	public void Show(int place, string score)
	{
		if (!initialized)
		{
			EventHelper.AddEventTrigger(doneButton.gameObject, UnityEngine.EventSystems.EventTriggerType.PointerDown, () => { buttonDown = true; Done (); });
			initialized = true;
		}

		//playerScore = (long)score; 

		placeText.text = place.ToString ("N0");
		scoreText.text = score; 
		
		gameObject.SetActive (true);
		
	}

	public void Hide()
	{
		gameObject.SetActive (false);
	}


	void Done()
	{
		SaveInitialsToXml (); 
		if (OnComplete != null)
			OnComplete ();
	}


	#region button events
	public void OnFirstInitialPress()
	{
		selected = Selected.first; 

		ResetSelectedImage (); 

		firstImage.sprite = selectedInitialSprite; 

	}

	public void OnMiddleInitialPress()
	{
		selected = Selected.middle; 

		ResetSelectedImage (); 

		middleImage.sprite = selectedInitialSprite; 
	}

	public void OnLastInitialPress()
	{
		selected = Selected.last; 

		ResetSelectedImage (); 

		lastImage.sprite = selectedInitialSprite; 
	}
	public void OnLeftButtonPress()
	{
		DoInitialInput (true); 
	}
	public void OnRightButtonPress()
	{
		DoInitialInput (false); 
	}
	#endregion 

	void ResetSelectedImage()
	{
		for (int i=0; i<initials.Length; i++) 
		{
			initials[i].sprite = normalInitialSprite; 
		}
	}

	string AdvanceLetter(bool left, char which)
	{
		if (left) {
			which--;
			if (which < 'A' && which > '9') {
				which = '9'; 
			}
			else if(which < '0' && which < 'Z')
			{
				which = 'Z'; 
			}
		} else {
			which++;
			if(which > 'Z')
			{
				which = '0';  
			}
			else if(which > '9' && which < 'A')
			{
				which = 'A'; 
			}
		}

		return which.ToString (); 
	}
	

	void DoInitialInput(bool left)
	{

		switch(selected)
		{
			case Selected.first:
				firstInitial.text = AdvanceLetter (left, firstInitial.text[0]);
				break; 
			case Selected.middle:
				middleInitial.text = AdvanceLetter (left, middleInitial.text[0]);
				break; 
			case Selected.last:
				lastInitial.text = AdvanceLetter(left, lastInitial.text[0]); 
				break;
			default:
				return;
				break;	
		}

	}

	public string GetInitials()
	{
		return firstInitial.text.Trim () + middleInitial.text.Trim () + lastInitial.text.Trim ();
	}

	#region XML Handling
	void PopulateInitialsFromXml()
	{
		firstInitial.text = SetDefaultInitial (gameFlow.settings.firstInitial); 
		middleInitial.text = SetDefaultInitial (gameFlow.settings.middleInitial); 
		lastInitial.text = SetDefaultInitial (gameFlow.settings.lastInitial); 
	}
	
	string SetDefaultInitial(string xmlInitial)
	{
		if(xmlInitial!=null)
		{
			return xmlInitial; 
		}
		else{
			return "A"; 
		}
		
	}

	void SaveInitialsToXml()
	{
		gameFlow.settings.firstInitial = firstInitial.text; 
		gameFlow.settings.middleInitial = middleInitial.text; 
		gameFlow.settings.lastInitial = lastInitial.text; 

		gameFlow.settings.Save (); 
	}
	#endregion


}
