﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InitialEntry : MonoBehaviour {

	public Text[] initials;
	public Text placeText;
	public Text scoreText;

	public Button leftButton;
	public Button rightButton;
	public Button doneButton;

	public float holdDelay = 1f;
	public float scrollTime = .8f;
	float holdTimestamp = 0f;
	float initialTimestamp = 0f;
	int selectedInitialIndex = 0;

	public Sprite normalInitialSprite;
	public Sprite selectedInitialSprite;

	bool initialized = false;

	bool buttonDown = false;

	public System.Action OnComplete;

	void Start()
	{
		//Show (21, 222222222);
	}

	public void Show(int place, ulong score)
	{
		if (!initialized)
		{
			EventHelper.AddEventTrigger(leftButton.gameObject, UnityEngine.EventSystems.EventTriggerType.PointerDown, () => { buttonDown = true; GoLeft (); });
			EventHelper.AddEventTrigger(rightButton.gameObject, UnityEngine.EventSystems.EventTriggerType.PointerDown, () => { buttonDown = true; GoRight (); });
			EventHelper.AddEventTrigger(doneButton.gameObject, UnityEngine.EventSystems.EventTriggerType.PointerDown, () => { buttonDown = true; Done (); });
			initialized = true;
		}

		selectedInitialIndex = 0;
		holdTimestamp = 0f;
		initialTimestamp = 0f;

		placeText.text = place.ToString ("N0");
		scoreText.text = score.ToString ("N0");

		gameObject.SetActive (true);

		initials [selectedInitialIndex].GetComponentInChildren<Image> ().sprite = selectedInitialSprite;
	}

	public void Hide()
	{
		gameObject.SetActive (false);
	}

	public void Update()
	{ 
		if (InputHelper.GetNumTouches() == 0)
		{
			holdTimestamp = 0f;
			initialTimestamp = 0f;
			buttonDown = false;
		}
		else if (!buttonDown)
		{
			bool left = InputHelper.IsTouching ((v) => { return v.x < .5f; });
			int amount = left ? -1 : 1;
			if (holdTimestamp == 0f)
			{
				holdTimestamp = Time.time;
				//Move the initial once.
				AdvanceLetter(amount);
			}
			else if (Time.time - holdTimestamp > holdDelay)
			{
				if (Time.time - initialTimestamp > scrollTime)
				{
					AdvanceLetter (amount);
					initialTimestamp = Time.time;
				}
			}
		}
	}

	public void AdvanceLetter(int amount)
	{
		char selectedInitial = initials [selectedInitialIndex].text [0];
		int newChar = (int)selectedInitial + amount;
		if (newChar > (int)'Z')
			newChar = (int)'A';
		else if (newChar < (int)'A')
			newChar = (int)'Z';
		selectedInitial = (char)newChar;
		initials [selectedInitialIndex].text = selectedInitial.ToString ();
	}

	public void AdvanceCurrent(int amount)
	{
		initials [selectedInitialIndex].GetComponentInChildren<Image> ().sprite = normalInitialSprite;

		selectedInitialIndex += amount;
		if (selectedInitialIndex < 0)
			selectedInitialIndex = initials.Length - 1;
		else if (selectedInitialIndex > initials.Length - 1)
			selectedInitialIndex = 0;

		initials [selectedInitialIndex].GetComponentInChildren<Image> ().sprite = selectedInitialSprite;
	}

	public void GoLeft()
	{
		AdvanceCurrent (-1);
	}

	public void GoRight()
	{
		AdvanceCurrent(1);
	}

	public void Done()
	{
		if (OnComplete != null)
			OnComplete ();
	}

	public string GetInitials()
	{
		return initials [0].text + initials [1].text + initials [2].text;
	}
}
