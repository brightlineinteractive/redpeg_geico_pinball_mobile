﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;
using System.Linq;
using System; 
using Prime31;

public class Leaderboard : MonoBehaviour
{
    private enum LeaderboardType { NONE, Local, World }

    public GameObject scoresPanel;
    public ScoreRecordUI scoreTemplate;
    public Scrollbar scrollbar;

    public Text typeLabel;
    public Button leftButton;
    public Button rightButton;

    public float LeaderboardLoadingTimeoutSecs = 20;

    public List<ScoreRecord> scores = null;
    public List<ScoreRecord> worldScores = new List<ScoreRecord>(0); //loaded from GameCenter/GPlay later

    public int maxScores = 10;

    public Menu.GameType currentDisplayedGameType = Menu.GameType.NONE;


    #region Public Methods/Functions

    public void Init(GameFlow g)
    {
        game = g;
        if (scores != null)
            return;
        scoreUIPool = new List<ScoreRecordUI>();
        LoadScoresFromFile();

    }

    public int AddScore(string initials, ulong score, Menu.GameType gType) //Returns placement in all scores.
    {
        if (scores == null)
            LoadScoresFromFile();

        ScoreRecord newRec = new ScoreRecord(initials, score, gType);

        int i = getPlaceIndex(score, gType);
        scores.Insert(i, newRec);

        File.AppendAllText(scoresFilePath, newRec.ToString() + '\n');
        removeLowestScores();

        submitWorldScore(initials, (long)score, gType);

        return i + 1; //0 isn't a place.
    }

    public int GetPlace(ulong score, Menu.GameType gType)
    {
        return 1 + getPlaceIndex(score, gType); //0 isn't a place (so add 1 to the index)
    }
    
    public void ChangeDisplayedType()
    {
        currentLeaderboardType = (currentLeaderboardType == LeaderboardType.Local ? LeaderboardType.World : LeaderboardType.Local);

        if (!isLoadingScores || currentLeaderboardType != LeaderboardType.World)
            displayScores();
    }

    #endregion


    GameFlow game;

    LeaderboardType currentLeaderboardType = LeaderboardType.Local;

    string scoresFilePath = null;
    List<ScoreRecordUI> scoreUIPool; //So we can reuse the score ui elements.

    string gameCenterName;
    string iOSLeaderboardId = @"GeicoPinballLeaderboard";

    //auto-generated by play dev console
    string androidLeaderboardId = @"CgkIxMKN4PoVEAIQAQ"; 
    string androidAppKey = "754505179460";

    bool isLoadingScores; //true while we are retreiving scores from the web

	ScoreRecord signedInScore; //world score for the player currently signed in to GameCenter/GooglePlay (null if not signed in, or not yet loaded, or no score exists)

	void Awake()
	{
        //I'm pretty sure this causes auto-boxing, so use this separate var to prevent a new object being added every time you hook into onClick
        changeDisplayTypeAction = delegate()
        {
            Debug.Log("Changing list. current=" + currentLeaderboardType + ", isLoading=" + isLoadingScores);
            ChangeDisplayedType();
        };
	}

	
	void OnEnable()
	{
		currentDisplayedGameType = game.menu.gameType;
		currentLeaderboardType = LeaderboardType.Local;
		
		DisplayOrHideArrows (); 
		
		if (!isLoadingScores)
			StartCoroutine(loadScoresRoutine()); //use coroutine to allow time for scores to load before display
		
		if (worldScores != null || currentLeaderboardType != LeaderboardType.World)
			displayScores();
	}
	

	void LoadScoresFromFile()
	{
		scoresFilePath = Application.persistentDataPath + "/scores.txt";
        scores = new List<ScoreRecord>();

        if (!File.Exists(scoresFilePath))
            return;

        string[] allScores = File.ReadAllText(scoresFilePath).Split('\n');
        foreach (string scoreLine in allScores)
        {
            if (string.IsNullOrEmpty(scoreLine))
                continue;

            ScoreRecord rec = ScoreRecord.FromString(scoreLine);
            if (rec != null)
            {
                scores.Add(rec);
                //scoresByGameType[(int)rec.gameType].Add (rec); //If the gametypes could ever change, this needs to be a string.
            }
        }

        if (scores.Count > 0)
            scores.Sort();
    }
	

    UnityEngine.Events.UnityAction changeDisplayTypeAction;

    void DisplayOrHideArrows()
    {
        try
        {
            leftButton.onClick.RemoveListener(changeDisplayTypeAction);
            rightButton.onClick.RemoveListener(changeDisplayTypeAction);
            Debug.Log("Removed onClick handlers.");
        }
        catch { }

        if (IsUserAuthenticated())
        {
            Debug.Log("user authenticated");
            leftButton.gameObject.SetActive(true);
            rightButton.gameObject.SetActive(true);

            leftButton.onClick.AddListener(changeDisplayTypeAction);
            rightButton.onClick.AddListener(changeDisplayTypeAction);
            Debug.Log("Added onClick handlers.");
        }
        else
        {
            leftButton.gameObject.SetActive(false);
            rightButton.gameObject.SetActive(false);
        }
    }


    #region GameCenter/GooglePlayServices leaderboard handling
	bool IsUserAuthenticated()
	{
        #if UNITY_EDITOR
        return true;
        #elif UNITY_IOS
		return GameCenterBinding.isPlayerAuthenticated(); 
		#elif UNITY_ANDROID
		return PlayGameServices.isSignedIn(); 
		#endif
	}


    IEnumerator loadScoresRoutine()
    {
        Debug.Log("Loading world scores...");
        isLoadingScores = true;
        yield return null;

        //get scores from whatever leaderboard (GPlay, iOS) we are using. (async operation)
        try
        {
            GetWorldScores();
            float t = Time.realtimeSinceStartup;
            bool sanityFail = false;
            while (isLoadingScores && !(sanityFail = (Time.realtimeSinceStartup - t) >= LeaderboardLoadingTimeoutSecs))
                yield return null;
            if (sanityFail)
            {
                Debug.LogError("Timeout connecting to leaderboard service.");
                worldScores = new List<ScoreRecord>(new[] { new ScoreRecord("Couldn't Retreive Scores", 0, Menu.GameType.Ball3) });
            }
            else 
                Debug.Log("Loaded scores count=" + worldScores.Count);
        }
        finally
        {
            isLoadingScores = false;
        }

        //now the scores (if any) should be ready for display
        displayScores();
    }


    void GetWorldScores()
    {
#if UNITY_IOS
		GameCenterBinding.retrieveScores (false, GameCenterLeaderboardTimeScope.AllTime, 1, 100, iOSLeaderboardId); 
		GameCenterManager.scoresLoadedEvent += scoresLoaded; 
#elif UNITY_ANDROID
        GPGManager.loadScoresFailedEvent += GPGManager_loadScoresFailedEvent;
        GPGManager.loadScoresSucceededEvent += GPGManager_loadScoresSucceededEvent;

		PlayGameServices.loadScoresForLeaderboard(androidLeaderboardId, GPGLeaderboardTimeScope.AllTime, false, false);
#else
        isLoadingScores = false;
#endif
    }


    private void submitWorldScore(string initials, long score, Menu.GameType gType)
    {
#if UNITY_IOS
		GameCenterBinding.reportScore (score, iOSLeaderboardId); 
#elif UNITY_ANDROID
        PlayGameServices.submitScore(androidLeaderboardId, score, initials);
#endif
    }



#if UNITY_IOS
	void scoresLoaded(GameCenterRetrieveScoresResult retrieveScoresResult)
	{
		Debug.Log ("SCORES LOADED " + retrieveScoresResult.scores.Count); 

		worldScores = new List<ScoreRecord> ();

		var scores = retrieveScoresResult.scores;
		string playerId = GameCenterBinding.playerIdentifier ();

		for(int i=0; i<scores.Count; i++)
		{
			ScoreRecord score = new ScoreRecord(scores[i].alias, (ulong)scores[i].value, Menu.GameType.Ball3); 
			if(scores[i].playerId == playerId)
				signedInScore = score;
			worldScores.Add (score); 
		}

		Debug.Log ("signedInPlayer=" + (signedInScore == null ? "nul" : signedInScore.ToString()));

        //tells the loadScoresRoutine it's ok to display the scores now
        isLoadingScores = false;
	}
#endif


#if UNITY_ANDROID

	//on failure, simply deregister all event hooks and log the cause
	void GPGManager_loadScoresFailedEvent(string leaderboardId, string errorString)
	{
		Debug.LogError("Couldn't load from leaderboardID=" + leaderboardId + ", err=" + errorString);
		GPGManager.loadScoresFailedEvent -= GPGManager_loadScoresFailedEvent;
		GPGManager.loadScoresSucceededEvent -= GPGManager_loadScoresSucceededEvent;
		GPGManager.loadScoresSucceededEvent -= GPGManager_loadedPersonalScore;
		isLoadingScores = false;
	}

	//called when Top Scores are loaded. Save the scores then begin trying to retreive the current user's world score 
    void GPGManager_loadScoresSucceededEvent(List<GPGScore> leaderboard)
    {
        worldScores = new List<ScoreRecord>(leaderboard.Count);
        foreach (var score in leaderboard)
            worldScores.Add(new ScoreRecord(score.displayName, (ulong)score.value, Menu.GameType.Ball3));

        Debug.Log("Score load succeeded. count=" + leaderboard.Count);
		GPGManager.loadScoresSucceededEvent -= GPGManager_loadScoresSucceededEvent;

		//register a different handler to use when the player's personal score is loaded
		GPGManager.loadScoresSucceededEvent += GPGManager_loadedPersonalScore;
		PlayGameServices.loadScoresForLeaderboard(androidLeaderboardId, GPGLeaderboardTimeScope.AllTime, false, true);
	}

	void GPGManager_loadedPersonalScore(List<GPGScore> leaderboard)
	{
		GPGPlayerInfo playerData = PlayGameServices.getLocalPlayerInfo();
		string playerId = playerData == null ? null : playerData.playerId;


        if (!string.IsNullOrEmpty(playerId))
            foreach (var score in leaderboard)
                if (score.playerId.Equals(playerId) && !string.IsNullOrEmpty(playerId))
                    signedInScore = new ScoreRecord(score.displayName, (ulong)score.value, Menu.GameType.Ball3);

        Debug.Log("LOADED PERSONAL SCORE player=" + (signedInScore == null ? "nul" : signedInScore.ToString()));

		isLoadingScores = false;
	}

#endif

    #endregion


    //get a zero-based index where the given score would fit within the sorted scores
    int getPlaceIndex(ulong score, Menu.GameType gType)
    {
        IEnumerable<ScoreRecord> scoresOfType;
        if (currentDisplayedGameType != Menu.GameType.NONE)
            scoresOfType = from s in scores where gType == currentDisplayedGameType select s;
        else
            scoresOfType = scores;
        List<ScoreRecord> scoreTypeList = scoresOfType.ToList();

        //Returns placement in all scores.
        int i = 0;
        while (i < scoreTypeList.Count && scoreTypeList[i].score >= score) i++;
        return i;
    }

    //get a type-string to display in the title of the leaderboard window
    string getLeaderboardTypeString(LeaderboardType leaderboardType)
    {
        string typestr = leaderboardType.ToString();
        return ((int)leaderboardType).ToString() != typestr ? typestr : "Error";
        //switch (leaderboardType)
        //{
        //    case LeaderboardType.NONE:
        //        return "NONE";
        //    case LeaderboardType.Local:
        //        return "Local";
        //    case LeaderboardType.World:
        //        return "World";
        //    default:
        //        return "Error";
        //}
    }


    ScoreRecordUI getScoreUI(int i)
    {
        if (scoreUIPool.Count > i)
        {
			scoreUIPool[i].gameObject.SetActive (true); //  all gobs returned by this func will have active=true 
            return scoreUIPool[i];
        }
        else
        {
            GameObject newObj = (GameObject)UnityEngine.Object.Instantiate(scoreTemplate.gameObject);
            ScoreRecordUI tempScoreUI = newObj.GetComponent<ScoreRecordUI>();
            scoreUIPool.Add(tempScoreUI);
            newObj.SetActive(true);
            newObj.transform.SetParent(scoresPanel.transform, false);
            return tempScoreUI;
        }
    }


    void removeLowestScores()
    {
        if (scores.Count > maxScores) {
			scores.RemoveRange(maxScores, (scores.Count-maxScores)); 
		}
		deleteLowestScoresFromFile(ScoreRecord.ToString (scores)); 
    }

	void deleteLowestScoresFromFile(List<string> dScores)
	{

		string tempFile = Application.persistentDataPath + "/temp.txt";
		
		using(var sw = new StreamWriter(tempFile))
		{

			for(int i=0; i<dScores.Count; i++){
				sw.WriteLine (dScores[i]); 
			}

		}
		
		File.Delete(scoresFilePath);
		File.Move(tempFile, scoresFilePath);
	}

    void displayScores()
    {
        Debug.Log("Display " + currentLeaderboardType.ToString() + "; isloading="+isLoadingScores+"; local,world=" + (scores == null ? "nul" : scores.Count.ToString()) + ", " + (worldScores == null ? "nul" : worldScores.Count.ToString())); 

        ScoreRecord[] scoresOfType;
        if (currentLeaderboardType == LeaderboardType.Local)
			scoresOfType = (from s in scores where s.gameType == Menu.GameType.Ball3 select s).ToArray ();
        else if (currentLeaderboardType == LeaderboardType.World)
        {
#if UNITY_IOS || UNITY_ANDROID
			scoresOfType = worldScores.ToArray();
#else
            Debug.Log("Not running on a mobile device.");
            scoresOfType = new ScoreRecord[0];
#endif
        }
        else
            scoresOfType = scores.ToArray();

		for (int i=0; i<maxScores; i++) 
		{
			if(i >= scoresOfType.Length) //past end of score list, so deactivate any pooled UI gobs and break out of the loop
			{
				if(i >= scoreUIPool.Count)
					break;
				scoreUIPool[i].gameObject.SetActive(false);
				continue;
			}

			ScoreRecordUI tempScoreUI = getScoreUI(i); //returns a gob that has already had SetActive(true) called on it
			tempScoreUI.SetScoreRecord(scoresOfType[i], i + 1);
            if (i % 2 == 1)
                tempScoreUI.SetAlternate();
            else
                tempScoreUI.SetDefault();

            if (currentLeaderboardType == LeaderboardType.World && signedInScore != null && scoresOfType[i].initials.Equals(signedInScore.initials))
                tempScoreUI.SetMyScore();
		}


        typeLabel.text = getLeaderboardTypeString(currentLeaderboardType) + " Leaderboard";

        StartCoroutine(SetScrollbar());
    }

    IEnumerator SetScrollbar()
    {
        yield return new WaitForSeconds(0.1f);
        scrollbar.value = 1f; //This doesn't work. Should probably put something in update.

    }


    void LoadTestData()
    {
        for (int i = 0; i < 40; i++)
        {
            AddScore("AAA", 10, Settings.instance.gameType);
            AddScore("ABA", 20, Settings.instance.gameType);
            AddScore("AAB", 30, Settings.instance.gameType);
            AddScore("ARC", 20, Settings.instance.gameType);
        }
    }







}
