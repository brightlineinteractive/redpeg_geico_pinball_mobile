﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System; 
using System.Collections.Generic; 

public class ScoreRecord : System.IComparable
{
	public System.DateTime timestamp;
	public string initials;
	public ulong score;
	public Menu.GameType gameType;

	public ScoreRecord()
	{
	}
	
	public ScoreRecord(string intls, ulong scr, Menu.GameType gType)
	{
		timestamp = System.DateTime.Now;
		initials = intls;
		score = scr;
		gameType = gType; 
	}
	
	public override string ToString ()
	{
		return string.Format ("{0}:{1}:{2}:{3}", timestamp.ToFileTime(), initials, score.ToString (), gameType.ToString());
	}
	
	static public ScoreRecord FromString(string str)
	{
		string[] parts = str.Split (':');

		try
		{
			ScoreRecord rec = new ScoreRecord ();
			rec.timestamp = System.DateTime.FromFileTime (long.Parse(parts [0]));
			rec.initials = parts[1];
			rec.score = ulong.Parse(parts[2]);
			rec.gameType = (Menu.GameType)Enum.Parse(typeof(Menu.GameType), parts[3]); 
			return rec;
		}
		catch (System.Exception ex)
		{
			Debug.LogError ("Error parsing score: " + ex.Message);
			return null;
		}
	}

	static public List<String> ToString(List<ScoreRecord> recs)
	{
		List<String> lines = new List<String>(); 
		for(int i=0; i<recs.Count; i++){
			lines.Add (string.Format ("{0}:{1}:{2}:{3}", recs[i].timestamp.ToFileTime(), recs[i].initials, recs[i].score.ToString (), recs[i].gameType.ToString())); 
		}

		return lines; 
	}
	
	public int CompareTo(object obj)
	{
		if (obj is ScoreRecord)
		{
			int scoreComp = score.CompareTo((obj as ScoreRecord).score)*-1;
			if (scoreComp == 0)
			{
				//Earliest comes first.
				return (obj as ScoreRecord).timestamp.CompareTo (timestamp);
			}
			else
			{
				return scoreComp;
			}
		}
		throw new System.ArgumentException ("Tried to compare ScoreRecord to " + obj.GetType ().Name);
	}
}

public class ScoreRecordUI : MonoBehaviour
{
	public Image colorImage;
	public Text rankText; 
	public Text nameText; 
	public Text scoreText; 
	public int charLength = 30;
    public Color highlightMyScoreColor;


	public void SetScoreRecord(ScoreRecord rec, int rank)
	{
		//Limit initials to 7 characters
		string initialsString = rec.initials.Substring(0, rec.initials.Length > 7 ? 7 : rec.initials.Length);
		string rankString = rank.ToString () + ".";
		/*while (rankString.Length <= 3) //For now, max rank is 3 digits long.
		{
			rankString = " " + rankString;
		}*/
		string scoreString = rec.score.ToString ();

		rankText.text = rankString; 
		nameText.text = rec.initials;  
		scoreText.text = scoreString; 

	}

    public void SetDefault()
    {
        rankText.color = nameText.color = scoreText.color = startingColor;
    }

	public void SetAlternate()
	{
		Color c = colorImage.color;
		c.a = .03f;
		colorImage.color = c;
        rankText.color = nameText.color = scoreText.color = startingColor;
    }

    public void SetMyScore()
    {
        rankText.color = nameText.color = scoreText.color = highlightMyScoreColor;
    }


    Color startingColor;

    void Awake()
    {
        startingColor = nameText.color;    
    }
}
