﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour {
	GameFlow game;

	public Slider soundVolumeSlider;
	public Slider musicVolumeSlider;
	public AudioSource testSound;

	public Button view1Button;
	public Button view2Button;

	bool soundChanged = false;
	bool musicChanged = false;
	
	public void Init(GameFlow g)
	{
		game = g;

		view1Button.onClick.AddListener(delegate{SetView(CameraControl.State.View1_ZoomedOut);}); 
		view2Button.onClick.AddListener(delegate{SetView(CameraControl.State.View2_ZoomedIn);}); 


	}

	void OnEnable()
	{
		SetView (game.settings.defaultView);
		soundVolumeSlider.value = game.GetSoundVolume ();
		musicVolumeSlider.value = game.GetMusicVolume ();
	}

	void OnDisable()
	{
		game.SaveSettings ();
	}

	public void OnSoundVolumeChanged()
	{
		soundChanged = true;
		game.SetSoundVolume (soundVolumeSlider.value);
	}

	public void OnMusicVolumeChanged()
	{
		musicChanged = true;
		game.SetMusicVolume (musicVolumeSlider.value);
	}

	void LateUpdate()
	{
		if (soundChanged && Input.GetMouseButtonUp (0))
			OnSoundRelease ();
		else if (musicChanged && Input.GetMouseButtonUp (0))
			OnMusicRelease ();
	}

	public void OnSoundRelease()
	{
		testSound.volume = soundVolumeSlider.value;
		testSound.Play ();
		soundChanged = false;
	}

	public void OnMusicRelease()
	{
		Debug.Log ("Music released");
		testSound.volume = musicVolumeSlider.value;
		testSound.Play ();
		musicChanged = false;
	}

	public void SetView(CameraControl.State view)
	{
		game.settings.defaultView = view;
		Debug.Log (view.ToString ());
		view1Button.interactable = (view != CameraControl.State.View1_ZoomedOut);
		view2Button.interactable = !view1Button.interactable;
	}
}
