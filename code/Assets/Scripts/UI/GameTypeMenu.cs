﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameTypeMenu : MonoBehaviour {

	Menu menu;
	GameFlow game;

	public Button[] buttons;
	public GameObject[] checkmarks;


	public void Init(Menu m, GameFlow ga)
	{
		menu = m;
		game = ga;
		for (int i = 0; i < buttons.Length; i++)
		{
			//buttons[i].tag = i;
			Menu.GameType g = (Menu.GameType)System.Enum.Parse(typeof(Menu.GameType), buttons[i].name);
			buttons[i].onClick.AddListener(delegate{menu.SelectGameType(g);});
		}

	}


	void OnDisable()
	{
		game.SaveSettings ();
	}
	
	//The Menu class calls this whenever the game type changes.
	public void SetSelectedGameType(Menu.GameType g)
	{
		Debug.Log(g.ToString());
		//Figure out which checkmark to show.
		for (int i = 0; i < buttons.Length; i++)
		{
			if (buttons[i] != null)
			{
				if (buttons[i].name == g.ToString())
				{
					buttons[i].interactable = false;
				}
				else
				{
					buttons[i].interactable = true;
				}
			}
		}
	}
}
