﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ResetScroll : MonoBehaviour {

	Scrollbar scrollbar;

	void OnEnable()
	{
		Debug.Log ("Enabling scroll!");
		if (scrollbar == null)
			scrollbar = GetComponent<Scrollbar> ();
		scrollbar.value = 1f;
	}
}
