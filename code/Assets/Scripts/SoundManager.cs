﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	public AudioClip BouncerClip1, BouncerClip2, ChuteClip, DropDownClip, FlipperClip, LightClip, LostBallClip; 
	public AudioClip RVClip, MotorcycleClip, DoorbellClip, CarClip, BoatClip; 
	public AudioClip MultiBallClip1, MultiBallClip2; 
	public AudioSource DoubleUpSource, MultiBallSource;
	
	public void PlayAudioClip(AudioClip audio)
	{
		if(audio.isReadyToPlay) {
			AudioSource.PlayClipAtPoint(audio,transform.position, Settings.instance.soundVolume );	
		}
	}
}
