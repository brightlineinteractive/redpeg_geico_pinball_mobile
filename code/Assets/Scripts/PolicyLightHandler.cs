﻿using UnityEngine;
using System.Collections;

public class PolicyLightHandler : MonoBehaviour {

	public GameObject groupHandler;
	private LightGroupHandler groupHandlerGC;
	public AudioClip sound;
	
	void Start () {
		if(groupHandler){
			groupHandlerGC=groupHandler.GetComponent<LightGroupHandler>();
		}
	}

	void OnTriggerEnter(){
		if(gameObject.activeSelf!=true){
			gameObject.SetActive(true);
			if(sound.isReadyToPlay) {
				AudioSource.PlayClipAtPoint(sound,transform.position, Settings.instance.soundVolume );		
			}
		}
		if(groupHandlerGC){
			groupHandlerGC.CheckLights();
		}
	}
}
