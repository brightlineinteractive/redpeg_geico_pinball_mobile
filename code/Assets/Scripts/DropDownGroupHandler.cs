﻿using UnityEngine;
using System.Collections;

public class DropDownGroupHandler : MonoBehaviour {
	
	public GameObject[] dropdowns;
	public float dropUpHeight;
	public float dropDownHeight;
	private GameFlow gameFlowGC;
	public AudioSource sound;
	public ScoreTextPopup scoreTextPopup; 

	ScoreboardAnimationHandler scoreboard; 
	
	void Start () {
		gameFlowGC=GameObject.Find("GameFlow").GetComponent<GameFlow>();
		scoreboard = GameObject.Find ("ScoreboardAnimationHandler").GetComponent<ScoreboardAnimationHandler> (); 
		for(int k =0; k<dropdowns.Length; k++){
			dropdowns[k].transform.localPosition =new Vector3(dropdowns[k].transform.localPosition.x ,dropUpHeight,dropdowns[k].transform.localPosition.z);
		}
	}
	
	public void CheckDropDowns(Collision collisionInfo){
		bool dropdownDown=true;
		for(int j=0; j<dropdowns.Length; j++){
			if(dropdowns[j].transform.localPosition.y!=dropDownHeight){
				dropdownDown=false;
				break;
			}
		}
		if(dropdownDown){
			//double the score
			//
			gameFlowGC.AddScore(0);
			gameFlowGC.score=gameFlowGC.score*2;
			if(!sound.isPlaying) {
				sound.PlayOneShot(sound.clip, Settings.instance.soundVolume); 	
			}
			scoreTextPopup.DoubleScoreText(collisionInfo.gameObject); 
			StartCoroutine(scoreboard.Animate(scoreboard.doubleUp)); 
			//Debug.Log("========= DOUBLED SCORE!!!! ===============");
			StartCoroutine(ResetDropDowns());
		}
	}
	public IEnumerator ResetDropDowns(){
		for(int k =0; k<dropdowns.Length; k++){
			dropdowns[k].transform.localPosition =new Vector3(dropdowns[k].transform.localPosition.x ,dropUpHeight,dropdowns[k].transform.localPosition.z);
			//yield return new WaitForSeconds(0.1f);
		}

		yield return null;
	}
}
