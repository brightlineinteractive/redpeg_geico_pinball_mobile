﻿using UnityEngine;
using UnityEngine.UI; 
using System.Collections;

public class RateQuotePopup : MonoBehaviour {

	public Menu menu; 
	string GeicoURL="https://www.geico.com/getaquote/"; 
	int hiddenButtonCounter=0; 
	int hitsToShow=4; 
	public Text numHitsText; 

	void OnEnable()
	{
		numHitsText.text = Settings.instance.rateQuoteHits.ToString () + " HITS"; 

	}
	public void OnClosePress()
	{
		gameObject.SetActive (false); 
		menu.Show (false, menu.howToPlay); 
	}
	
	public void GetRateQuote()
	{
		Resources.UnloadUnusedAssets (); 
		Settings.instance.rateQuoteHits++; 
		Application.OpenURL (GeicoURL); 
		Settings.instance.Save (); 
		//numHitsText.text = Settings.instance.rateQuoteHits.ToString () + " HITS"; 
	}
	
	public void HiddenButtonHit()
	{
		hiddenButtonCounter++; 
		if (hiddenButtonCounter > hitsToShow) {
			gameObject.SetActive(true); 
			hiddenButtonCounter=0; 
		}
	}
}
