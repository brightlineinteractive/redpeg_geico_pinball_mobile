﻿using UnityEngine;
using System.Collections;

public class LightHandler : MonoBehaviour {

	public GameObject lightObject;
	public GameObject groupHandler;
	private LightGroupHandler groupHandlerGC;
	public int scoreToAdd=500;
	public AudioClip sound;
	
	void Start () {
		if(groupHandler){
			groupHandlerGC=groupHandler.GetComponent<LightGroupHandler>();
		}
	}

	void OnTriggerEnter(){
		if(lightObject.activeSelf!=true){
			lightObject.SetActive(true);
			AudioSource.PlayClipAtPoint(sound,transform.position, Settings.instance.soundVolume);	
		}

		if(groupHandlerGC){
			groupHandlerGC.CheckLights();
		}
	}
}
