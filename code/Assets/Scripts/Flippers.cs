﻿using UnityEngine;
using System.Collections;

public class Flippers : MonoBehaviour {

	//this script is used by the right flipper
	
	//variable to hold if the RightFLipper command button is pressed
	private bool  button = false;
	private Collider other;
	public float trailSeconds=1;
	public bool isEnabled=true;
	private GameFlow gameFlow;
	
	private Vector3 defaultVel; 
	private Quaternion defaultRot; 
	private Quaternion pressedRot; 

	public float force=7500; 

	bool isLeftFlipper;
	bool isRightFlipper; 

	bool leftButtonPressed;
	bool rightButtonPressed; 

	float leftCounter=1f; 
	float rightCounter=1f; 

	void Start ()
	{
		gameFlow = GameObject.Find ("GameFlow").GetComponent<GameFlow> ();
		defaultVel = new Vector3 (0, GetComponent<Rigidbody>().angularVelocity.y, 0); 

		GetLeftOrRightFlipper (); 

	}
	
	void Update()
	{
		if(isLeftFlipper)
		{
			leftButtonPressed = DoFlipperInput("LeftFlipper", InputHelper.leftFlipper, leftCounter); 
		}
		if(isRightFlipper)
		{
			rightButtonPressed = DoFlipperInput("RightFlipper", InputHelper.rightFlipper, rightCounter); 
		}

		Debug.Log (isLeftFlipper + "," +  leftButtonPressed + "," + isRightFlipper + "," + rightButtonPressed); 
	}
	void FixedUpdate ()
	{
		DoFlipperPhysics (leftButtonPressed, leftCounter); 
		DoFlipperPhysics (rightButtonPressed, rightCounter); 
		
		
	}	
	void OnCollisionEnter (Collision collisionInfo) 
	{
		
		//get the collider which is colliding with the bouncers 
		other = collisionInfo.collider;
		//if the colliding object is the ball , add some points to the score
		if(other.gameObject.CompareTag("Ball")){
			other.gameObject.transform.FindChild("Trail").gameObject.SetActive(true);
			StartCoroutine(DeactivateCo());
		}
	}		

	void GetLeftOrRightFlipper()
	{
		
		if (this.transform.parent.name.Contains ("Left")) 
		{
			isLeftFlipper=true; 
		}
		if(this.transform.parent.name.Contains ("Right"))
		{
			isRightFlipper=true; 
		}

	}

	bool DoFlipperInput(string whichKey, bool whichTouch, float counter)
	{
		bool pressed = false; 
		//if the fLipper command button is pressed,set the variable acordingly
		if (Input.GetButton (whichKey) && (gameFlow.inGame)) {
			counter -= Time.deltaTime; 
			pressed=true; 
		} else if (whichTouch) {
			counter -= Time.deltaTime; 
			pressed=true; 
		} else {
			counter = 1f; 
			pressed=false; 
		}

		Debug.Log (pressed); 

		return pressed; 
	}

	void DoFlipperPhysics(bool button, float counter)
	{
		if (button) 
		{
			//reset the velocities on the unused axes to zero, to avoid any strange behaviour
			//add the required torque to move the flipper down, until the limits of the hinge joint
			if(transform.rotation.y < pressedRot.y && counter>=0){
				GetComponent<Rigidbody>().angularVelocity = defaultVel;
				GetComponent<Rigidbody>().AddRelativeTorque(new Vector3 (0, force * Time.fixedDeltaTime, 0),ForceMode.VelocityChange);
				GetComponent<Rigidbody>().isKinematic=false; 
			}
			else{
				//transform.rotation = pressedRot; 
				GetComponent<Rigidbody>().isKinematic=true; 
			}
			
			
			//rigidbody.AddRelativeTorque(Vector3(0,7500 ,0),ForceMode.VelocityChange);
		}
		
		//if the RightFLipper command button isnt pressed
		else
		{
			GetComponent<Rigidbody>().isKinematic=false; 
			//reset the velocities on the unused axes to zero, to avoid any strange behaviour
			GetComponent<Rigidbody>().angularVelocity = defaultVel; 
			//add the required torque to move the flipper down, until the limits of the hinge joint
			GetComponent<Rigidbody>().AddRelativeTorque(new Vector3 (0, -1*force * Time.fixedDeltaTime, 0),ForceMode.VelocityChange);
			//rigidbody.AddRelativeTorque(Vector3(0,-7500 ,0),ForceMode.VelocityChange);
		}	
	}

	private IEnumerator DeactivateCo()
	{
		yield return new WaitForSeconds(trailSeconds);
		other.gameObject.transform.FindChild("Trail").gameObject.SetActive(false);
	}
}
