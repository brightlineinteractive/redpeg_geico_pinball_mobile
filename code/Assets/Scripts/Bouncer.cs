﻿using UnityEngine;
using System.Collections;

public class Bouncer : MonoBehaviour {

	//variable to hold how much points to give when the ball touches them
	public float pointsToGive = 100;

    //how much force does the bumper apply to the ball?
    public float BounceAddedForce = 0;

	//variable to hold the collider which is colliding with the bouncers 
	private Collider other;
	public AudioClip flipperKicks;
	private GameFlow gameFlowGC;
	
	void Start(){
		gameFlowGC=GameObject.Find("GameFlow").GetComponent<GameFlow>();
	}
	
	void OnCollisionEnter (Collision collisionInfo ) 
	{
		//get the collider which is colliding with the bouncers 
		other = collisionInfo.collider;
		//if the colliding object is the ball , add some points to the score
		if(other.gameObject.CompareTag("Ball")){
			gameFlowGC.SetScorePoints((int)pointsToGive, this.gameObject);
			if(flipperKicks.isReadyToPlay) {
				AudioSource.PlayClipAtPoint(flipperKicks,transform.position, Settings.instance.soundVolume );	
			}
            //bounce back the ball
			if(BounceAddedForce > 0)
			{
	            Vector3 forceSum = Vector3.zero;
	            foreach (var c in collisionInfo.contacts)
	                forceSum -= c.normal.normalized;
	            forceSum = forceSum * (1f / collisionInfo.contacts.Length);
	            other.GetComponent<Rigidbody>().AddForce(forceSum * BounceAddedForce);
			}
		}
		else
		{
			Physics.IgnoreCollision(collisionInfo.collider,this.GetComponent<Collider>());
		}
	}
}
