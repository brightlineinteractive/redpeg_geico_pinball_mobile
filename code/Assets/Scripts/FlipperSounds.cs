﻿using UnityEngine;
using System.Collections;

public class FlipperSounds : MonoBehaviour {

	public AudioClip flipperKicks; 

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetButtonDown ("LeftFlipper") || Input.GetButtonDown ("RightFlipper")) {
			if (flipperKicks.isReadyToPlay) {
				AudioSource.PlayClipAtPoint (flipperKicks, transform.position, Settings.instance.soundVolume);
				
			}
		}
	}
}
