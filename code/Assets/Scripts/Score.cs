﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {

	//varible to hold how many points do we have
	public float scorePoints = 0;
	//variable to hold how many lives do we have
	public float lives = 3;
	//variable to hold if its game over
	private bool showGameOver = false;
	
	void Update()
	{
		//if lives equal 0 , then game over
		if(lives == 0)
		{
			showGameOver = true;
			StartCoroutine( ReloadLevel());
		}
	}
	
	void OnGUI ()
	{	
		//draw the controls instruction window
		GUILayout.BeginArea(new Rect(10,10,150,120),"Controls",GUI.skin.window);
		GUILayout.Label("Move the Flippers with the Arrows");
		GUILayout.Label("Launch the Ball with Space Bar");
		GUILayout.Label("Press A key for tilt");
		GUILayout.Label("Press Esc To Exit or Restart");
		GUILayout.EndArea();
		//draw the score window
		GUILayout.BeginArea(new Rect(Screen.width  - 230 ,10,220,80),"Score",GUI.skin.window);
		GUILayout.Label("Points : " + scorePoints.ToString());
		GUILayout.Label("Balls  : " + lives.ToString());
		GUILayout.EndArea();
		
		//if its gameover show the game over window
		if(showGameOver)
		{
			GUILayout.BeginArea(new Rect((Screen.width / 2) - 50,(Screen.height / 2) - 30,100,60),GUI.skin.box);
			GUILayout.Label("    GAME OVER");
			GUILayout.Label("Your Score is : " + scorePoints.ToString());
			GUILayout.EndArea();
		}
	}
	
	public IEnumerator ReloadLevel()
	{
		//if its game over, wait 5 seconds and then reload the level
		if(showGameOver)
		{
			yield return new WaitForSeconds (5);
			Application.LoadLevel(Application.loadedLevel);
		}
	}
}
