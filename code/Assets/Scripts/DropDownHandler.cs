﻿using UnityEngine;
using System.Collections;

public class DropDownHandler : MonoBehaviour {

	public GameObject groupHandler;
	private DropDownGroupHandler groupHandlerGC;
	private Collider other;
	private GameFlow gameFlowGC;
	public int pointsToGive=50;
	public AudioClip flipperKicks;
	
	void Start () {
		if(groupHandler){
			groupHandlerGC=groupHandler.GetComponent<DropDownGroupHandler>();
		}
		gameFlowGC=GameObject.Find("GameFlow").GetComponent<GameFlow>();
	}
	void OnCollisionEnter (Collision collisionInfo) 
	{
		//get the collider which is colliding with the bouncers 
		other = collisionInfo.collider;
		//if the colliding object is the ball , add some points to the score
		if(other.gameObject.CompareTag("Ball")){
			gameFlowGC.SetScorePoints(pointsToGive, this.gameObject);

			if(flipperKicks.isReadyToPlay) {
				AudioSource.PlayClipAtPoint(flipperKicks,transform.position, Settings.instance.soundVolume );		
			}
			//Debug.Log(transform.localPosition.y);
			if(transform.localPosition.y!=groupHandlerGC.dropDownHeight){
				transform.localPosition=new Vector3(transform.localPosition.x,groupHandlerGC.dropDownHeight,transform.localPosition.z);
			}
			groupHandlerGC.CheckDropDowns(collisionInfo);
		}
	}


}
