﻿using UnityEngine;
using System.Collections;

public class LeftFlipper : MonoBehaviour {

	
	//variable to hold if the RightFLipper command button is pressed
	public bool  button = false;
	private Collider other;
	public float trailSeconds=1;
	public bool isEnabled=true;
	public bool isTouchingBall=false;  
	private GameFlow gameFlow;

	private Vector3 defaultVel; 

	public GameObject Ball; 
	public GameObject Ball2; 
	public GameObject Ball3; 

	public float force=7500; 

	//float counter=2f;

	Quaternion pressedRot; 

	[Header("Joint Motor")]
	public HingeJoint Hinge;
	public JointMotor JointMotor = new JointMotor();
	public float JointMotorForce = 1000;
	public float JointMotorTargetVelocity = 1000;


	void Start ()
	{
		gameFlow = GameObject.Find ("GameFlow").GetComponent<GameFlow> ();
		defaultVel = new Vector3 (0, GetComponent<Rigidbody>().angularVelocity.y, 0); 
		
	}
	

	void Update()
	{
		//Debug.Log (Input.mousePosition.x / Screen.width);
		//if the LeftFLipper command button is pressed,set the variable acordingly
		if ((Input.GetButton("LeftFlipper"))&&(gameFlow.inGame))
		{
			button = true;

		}
		else if (InputHelper.leftFlipper&&gameFlow.inGame&&!gameFlow.isPaused)
		{
			button = true;
		}
		else
		{
			button = false;
		}
		
		
	}
	void FixedUpdate ()
	{
		//if the RightFLipper command button is pressed
		
		if (button) 
		{
			if (Hinge != null) {
				JointMotor.force = JointMotorForce;
				JointMotor.targetVelocity = JointMotorTargetVelocity;
				Hinge.motor = JointMotor;
				Hinge.useMotor = true;
			}
			else
			{
				
			if(!GetComponent<Rigidbody>().isKinematic)
				GetComponent<Rigidbody>().angularVelocity = defaultVel; 
			//add the required torque to move the flipper down, until the limits of the hinge joint
			GetComponent<Rigidbody>().AddRelativeTorque(new Vector3 (0, force*-1 * Time.fixedDeltaTime, 0),ForceMode.VelocityChange);
			//rigidbody.AddRelativeTorque(Vector3(0,7500 ,0),ForceMode.VelocityChange);
			}
		}
		
		//if the RightFLipper command button isnt pressed
		else
		{
			if (Hinge != null) {
				if (Hinge.useMotor) {
					Hinge.useMotor = false;
				}
			}

			GetComponent<Rigidbody>().isKinematic=false; 
			//reset the velocities on the unused axes to zero, to avoid any strange behaviour
			GetComponent<Rigidbody>().angularVelocity = defaultVel; 
			//add the required torque to move the flipper down, until the limits of the hinge joint
			GetComponent<Rigidbody>().AddRelativeTorque(new Vector3 (0, force * Time.fixedDeltaTime, 0),ForceMode.VelocityChange);
			//rigidbody.AddRelativeTorque(Vector3(0,-7500 ,0),ForceMode.VelocityChange);
		}	
		
		
		
	}	
	void OnCollisionEnter (Collision collisionInfo) 
	{
		
		//get the collider which is colliding with the bouncers 
		other = collisionInfo.collider;
		///////// ***** trail code, this is not used for mobile
		//if the colliding object is the ball , add some points to the score
		/*if((other.gameObject==Ball)||(other.gameObject==Ball2)||(other.gameObject==Ball3)){
			
			other.gameObject.transform.FindChild("Trail").gameObject.SetActive(true);
			StartCoroutine(DeactivateCo());
		}*/
		if (other.CompareTag ("Ball")) {
			isTouchingBall = true; 
			if(other.gameObject.GetComponent<Rigidbody>().velocity.magnitude<.75f)
			{
				gameObject.GetComponent<Rigidbody>().isKinematic=true; 
			}
		}
		else{
			isTouchingBall = false; 
		}
	}

	void OnCollisionExit(Collision collisionInfo)
	{
		if (collisionInfo.collider.CompareTag ("Ball")) {
			isTouchingBall = false; 
		}
	}
	

	private IEnumerator DeactivateCo()
	{
		yield return new WaitForSeconds(trailSeconds);
		other.gameObject.transform.FindChild("Trail").gameObject.SetActive(false);
	}
}
