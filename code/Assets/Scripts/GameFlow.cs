﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using UnityEngine.UI;
using HutongGames.PlayMaker;
using HutongGames.PlayMaker.Actions;
using Prime31; 

public class GameFlow : MonoBehaviour {

    public enum Location
    {
        NONE = 0,
        Bus,
        Kiosk
    }

	#if UNITY_IOS
	public const string versionNumber = ""; 
	#elif UNITY_ANDROID
	public const string versionNumber = ""; 
	#endif

	[Header("GameObjects")]
	public CameraControl cameraControl;
	public GameObject attractLightSequence;
	public GameObject ball;
	public GameObject ball2;
	public GameObject ball3;
	public GameObject movieControl;
	public GameObject ballMultiSpawn;
	public GameObject ballStart;
	public GameObject[] lights;
	public GameObject[] leftFlippers;
	public GameObject[] rightFlippers;
	public AudioSource gameIntroMusic;
	public AudioSource gameLoopMusic;
    public float MusicFadeInTime = 1;
	public AudioSource gameIntroSound; 
	public GameObject xmlLoader;
	public GameObject testRailSpawnPoint; 

    public int secondsPerGame=75;
	public int ballsPerGame=3;
	public int scoreMultiplier=1;
	public float startScreenDelay=2f;
	public ulong score=0;
	public DropDownGroupHandler[] dropdownGroups;
	public bool inGame=false;
	public Leaderboard leaderboard;

	public Text buildNumber;
	public Text scoreLabel;
	public Text counterLabel;
    public int ballsLeft = 3;
	public TrapHandler trapHandler;

    public Settings settings;

    public int multiplierLimit = 9;

    public float timeToRespawnBall = 3.0f;

    [Space(5)]
    public PlayMakerFSM trafficControl01;
    public PlayMakerFSM trafficControl02;
    public PlayMakerFSM trafficControl03;


    public GameObject pauseButton;
    public GameObject cameraButton;
    public Menu menu;

    public LeaderboardEntry leaderboardEntryUI;

    public bool isPaused = false;
    public bool stressTestMode = false;

    public GameObject[] balls;

    public Text getReady;

    public BallLauncher launcher;



	string locHolder = "Kiosk";

	float originalMusicIntroVolume = 1f;
	float originalMusicLoopVolume = 1f;

    int currentGameSeconds = 0;
    float gameEndTimestamp = 0f;

    int secondsToStartKiosk = 2;
    int startButtonHitCount = 0;

    GameObject ball1Trail;
    GameObject ball2Trail;
    GameObject ball3Trail;

    float resetTimer;

	bool timedGame; 
	int initialSeconds;

    Vector3[] storedVelocities = new Vector3[3];
    Vector3[] storedAngularVelocities = new Vector3[3]; 


    dontgothroughtthing scriptVar;
    public Vector3 BallStartPos;
    Vector3 MultiBallPos;

    ScoreTextPopup scoreTextPopup;

    bool isNewGame;
    bool isPausing;

    ScoreboardAnimationHandler scoreboard;

    float fadeInMult = 0; //climbs to 1 at a rate determined by MusicFadeInTime

	void Awake () 
	{
        gameLoopMusic.gameObject.SetActive(true);
        originalMusicIntroVolume = gameIntroMusic.volume;
        originalMusicLoopVolume = gameLoopMusic.volume;

        //We want intro music to fade in so start at vol=0 (now that we stored the orig value)
        gameLoopMusic.volume = 0;


		Time.timeScale = 1f;

		settings = Settings.Load ();
		SetMusicVolume (settings.musicVolume);
		SetSoundVolume (settings.soundVolume);
		menu.gameType = Menu.GameType.Ball3; 

		leaderboard.Init (this);
		menu.Init (this);

		leaderboardEntryUI.OnComplete = GameOver;

		Input.multiTouchEnabled = true;
		Input.simulateMouseWithTouches = true;



		//TouchScreenKeyboard.Open ("");
		//XMLLoader.instance.getConfigSetting ("GameSettings/Time", secondsPerGame, out secondsPerGame);
		//OptionsManager.LoadSettings += LoadSettings;
		Application.targetFrameRate = 60;
		attractLightSequence.SetActive(true);
		trapHandler=GameObject.Find("TrapHandler").GetComponent<TrapHandler>();


		// turn off the GameOverUI
		//TurnOffGameOverUI (false);
		//
		//TurnOffStartGameUI ();
		//
		ball1Trail = ball.transform.FindChild("Trail").gameObject;
		ball2Trail = ball2.transform.FindChild("Trail").gameObject;
		ball3Trail = ball3.transform.FindChild("Trail").gameObject;

		BallStartPos = ballStart.transform.position; 
		MultiBallPos = ballMultiSpawn.transform.position;

		scoreTextPopup = GameObject.Find ("ScoreTextHandler").GetComponent<ScoreTextPopup> (); 
		scoreboard = GameObject.Find ("ScoreboardAnimationHandler").GetComponent<ScoreboardAnimationHandler> (); 

		//float outValue;
		//XMLLoader.instance.getConfigSetting ("GameSettings/Bus/InitialsTimeout", 30f, out outValue);
		//initialsInput.GetComponent<InitialsInput> ().SetTimeout (outValue);
		//XMLLoader.instance.getConfigSetting ("GameSettings/Bus/InitialsBackOpacity", .6f, out outValue);
		//initialsInput.GetComponent<InitialsInput> ().SetBackOpacity (outValue);

		//FsmVariables.GlobalVariables.GetFsmFloat ("SoundEffectVolume").Value = 1;
		//Debug.Log ("Sound Effect Volume: " + FsmVariables.GlobalVariables.GetFsmFloat ("SoundEffectVolume").Value);
        balls = new GameObject[] { ball, ball2, ball3 };
		menu.Show (true);

		isPausing = false; 
		buildNumber.text = versionNumber; 
		Resources.UnloadUnusedAssets (); 
		System.GC.Collect (); 
	}


	public void SetGameTypeTimed(int seconds)
	{
		timedGame = true; 
		initialSeconds = seconds;
		ballsPerGame = int.MaxValue; 
		stressTestMode = false;
	}

	public void SetGameTypeBalls(int balls)
	{
		timedGame = false;
		initialSeconds = 0; 
		ballsPerGame = balls;
		stressTestMode = false;
	}

	public void SetGameTypeStressTest()
	{
		if (stressTestMode)
			stressTestMode = false;
		else 
			stressTestMode = true; 

		initialSeconds = int.MaxValue;
		StartCoroutine (DoStressTest ()); 
	}



	bool IsTouchingStart()
	{
		return InputHelper.start;
	}


    void LateUpdate()
    {
        if (fadeInMult < 1)
        {
            fadeInMult = Mathf.Clamp01(fadeInMult + (Time.deltaTime / MusicFadeInTime));
            gameLoopMusic.volume = originalMusicLoopVolume * settings.musicVolume * fadeInMult;
        }

        if (!inGame)
        {
            if (gameEndTimestamp > 0f)
            {
                if (Time.time - gameEndTimestamp >= startScreenDelay)
                {
                    gameEndTimestamp = 0f;
                    menu.Show();
                }
            }
        }
        else if (stressTestMode)
        {
            if (currentGameSeconds % 3 == 0)
            {
                ActivateBall();
            }
        }

        if (InputHelper.touchBall != null)
            DoTilt(InputHelper.touchBall);
    }

	public void ForceRespawn(){
		ball.SetActive(false);
		ball2.SetActive(false);
		ball3.SetActive(false);
		
		Respawn();
		
	}
	public void Respawn(){

		int ballsLeftScore; 

		if(inGame && !stressTestMode){
			Debug.Log("trigger respawn");
			if((ball.activeSelf)||(ball2.activeSelf)||(ball3.activeSelf))
			{
				Debug.Log("not respawning, ball still on board");
				//do nothing if there is still a ball on the board
			} 
			else 
			{
				if(ballsLeft > 0)
				{
					ballsLeft--;
					Debug.Log ("Seconds: " + secondsPerGame + "Balls: " + ballsLeft); 
					ResetMultiplier();
					InitializeBall(ball, BallStartPos); 
					
					trapHandler.OpenDoors();

					if(menu.gameType==Menu.GameType.Ball1||menu.gameType==Menu.GameType.Ball3)
					{
						ballsLeftScore = ballsLeft + 1; 
						if(ballsLeftScore==1)
							counterLabel.text = ballsLeftScore.ToString () + " BALL"; 
						else
							counterLabel.text = ballsLeftScore.ToString () + " BALLS"; 
					}
				} 
				else {
					EndGame();	
					counterLabel.text = "0 BALLS"; 
				}
			}
		} 
		else 
		{
			Debug.Log("not respawning");
		}


	}


    public void ActivateBall()
    {
        if (inGame)
        {
            //Debug.Log("Activate Ball");
            if (ball.activeSelf == false)
            {
                //Debug.Log("Activate Ball 1");
                ball.transform.FindChild("Trail").gameObject.SetActive(false);
                InitializeBall(ball, MultiBallPos);
                ball.transform.FindChild("Trail").gameObject.SetActive(true);
                ball.SetActive(true);
                StartCoroutine(TurnOffTrail(1));


            }
            else if (ball2.activeSelf == false)
            {
                //Debug.Log("Activate Ball 2");
                MultiBallPos.x = MultiBallPos.x + .01f;
                ball2.transform.FindChild("Trail").gameObject.SetActive(false);
                InitializeBall(ball2, MultiBallPos);
                ball2.transform.FindChild("Trail").gameObject.SetActive(true);
                ball2.SetActive(true);
                StartCoroutine(TurnOffTrail(2));

            }
            else if (ball3.activeSelf == false)
            {
                //Debug.Log("Activate Ball 3");
                MultiBallPos.x = MultiBallPos.x + .02f;

                ball3.transform.FindChild("Trail").gameObject.SetActive(false);
                InitializeBall(ball3, MultiBallPos);
                ball3.transform.FindChild("Trail").gameObject.SetActive(true);
                ball3.SetActive(true);
                StartCoroutine(TurnOffTrail(3));
            }
            else
            {
                //Debug.Log("All Balls Active");
            }
        }
    }

	
	public void ActivateLightsForSeconds(float waitTime){
		StartCoroutine(ActivateLightsCoRoutine(waitTime));
	}
	
	public void IncreaseMultiplier(){
		StartCoroutine(scoreboard.Animate (scoreboard.multiplier)); 
		scoreTextPopup.MultiplyText ();
		if(scoreMultiplier<multiplierLimit)
			scoreMultiplier++;
		Debug.Log ("Mulitplier increased to " + scoreMultiplier);
	}
	public void ResetMultiplier(){
		scoreMultiplier=1;
	}

    public void SetScorePoints(int value, GameObject scoreObj)
    {
        scoreTextPopup.AddScoreText(value, scoreObj);
    }

    public ulong AddScore(ulong value)
    {
        if (inGame)
        {
            ulong tempScore = value;
            // addind becasue casting on ulong is causing issues?
            tempScore = tempScore * (ulong)scoreMultiplier;

            //Debug.Log("adding score "+value+"X" + scoreMultiplier + "," + (ulong)scoreMultiplier +" to existing score("+score+") = " + (score + tempScore).ToString());
            score += tempScore;
            if (scoreLabel)
            {
                scoreLabel.text = FormatScore(score);
            }
            return tempScore;
        }

        return 0;
    }

	public string ConvertScoreToTruncatedString(float score, float amount, int maxChars, string letter)
	{
		float dividedScore; 
		string formatted; 
		
		dividedScore = score / amount; 
		formatted = dividedScore.ToString (); 
		formatted = Truncate (formatted, maxChars) + letter; 
		
		return formatted; 
		
	}

	public void ActivateLights(){
		attractLightSequence.SetActive(true);
	}
	public void DeactivateLights(){
		attractLightSequence.SetActive(false);
		for(int light=0; light<lights.Length; light++){
			lights[light].SetActive(false);
		}
	}

    public void StartGame()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

		InputHelper.isEnabled = true;  

        launcher.isEnabled = true;

        trafficControl01.SendEvent("StartGame");
        trafficControl02.SendEvent("StartGame");
        trafficControl03.SendEvent("StartGame");

        startButtonHitCount = 0;
        secondsPerGame = initialSeconds;
        inGame = true;
        DeactivateLights();
        score = 0;
        AddScore(0);
        scoreMultiplier = 1;
        resetTimer = 0f;
        ballsLeft = ballsPerGame;

        if (!stressTestMode)
        {
            ForceRespawn();
            menu.SelectGameType(menu.gameType);
        }
        else
            StartCoroutine(DoStressTest());
        if (timedGame)
            StartCoroutine(GameTimer());


        for (uint i = 0; i < dropdownGroups.Length; i++)
        {
            dropdownGroups[i].StartCoroutine("ResetDropDowns");
        }
        StartCoroutine(DoStartupSounds());


        menu.Hide();

        //cameraControl.lastState = Settings.instance.defaultView;
        cameraControl.currentState = Settings.instance.defaultView;

        isNewGame = true;

        Resume();

        isPausing = false;
    }


    public void SetMusicVolume(float volume)
    {
        settings.musicVolume = volume;
        //gameIntroMusic.volume = volume * originalMusicIntroVolume * fadeInMult;
        gameLoopMusic.volume = volume * originalMusicLoopVolume * fadeInMult;
    }

    public float GetMusicVolume()
    {
        return settings.musicVolume;
    }

    public void SetSoundVolume(float volume)
    {
        settings.soundVolume = volume;
        PlaySound.hackVolume = volume;
        PlayRandomSound.hackVolume = volume;
    }

    public float GetSoundVolume()
    {
        return settings.soundVolume;
    }


    public void Pause()
    {
        StopCoroutine("GetReadyAfterPause");
        StopCoroutine("DoGetReadyText");
		InputHelper.isEnabled = false; 
        getReady.gameObject.SetActive(false);
        isPaused = true;
        isPausing = true;
        try
        {
            for (int i = 0; i < balls.Length; i++)
            {
                storedVelocities[i] = balls[i].GetComponent<Rigidbody>().velocity;
                storedAngularVelocities[i] = balls[i].GetComponent<Rigidbody>().angularVelocity;
                balls[i].GetComponent<Rigidbody>().Sleep();
                balls[i].GetComponent<Rigidbody>().isKinematic = true;

            }
        }
        catch (NullReferenceException e)
        {
            Debug.Log(e);
        }

        menu.Show(true);

    }

    public void Resume()
    {
        isPaused = false;
        menu.Hide();

        StartCoroutine("GetReadyAfterPause");
        if (!inGame)
            StartGame();
    }



    public void TogglePause()
    {
        if (!isPausing)
        {
            if (isPaused)
                Resume();
            else
                Pause();
        }
    }

    public void GameOver()
    {
        isPaused = true;
        //Time.timeScale = 0f;

        for (int i = 0; i < balls.Length; i++)
        {
            storedVelocities[i] = balls[i].GetComponent<Rigidbody>().velocity;
            balls[i].GetComponent<Rigidbody>().Sleep();
        }

        pauseButton.SetActive(false);
        cameraButton.SetActive(false);
        menu.Show(false, menu.gameOverMenu);
        leaderboard.AddScore(leaderboardEntryUI.GetInitials(), score, menu.gameType);
        leaderboardEntryUI.Hide();
    }

    public void SaveSettings()
    {
        if (settings != null)
        {
            settings.gameType = menu.gameType; //Remember the game type.
            //future: remember initials?
            settings.Save();
        }
    }






    IEnumerator DoStressTest()
    {
        if (stressTestMode && !isPaused)
        {
            if (currentGameSeconds % 3 == 0)
            {
                ActivateBall();
            }
        }
        yield return new WaitForSeconds(0.2f);
        StartCoroutine(DoStressTest());
    }


    public void InitializeBall(GameObject ballObject, Vector3 startPos)
    {
        ballObject.SetActive(true);
        scriptVar = ball.GetComponent<dontgothroughtthing>();
        scriptVar.enabled = false;
        if (!ballObject.GetComponent<Rigidbody>().isKinematic)
        {
            ballObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            ballObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }
        ballObject.transform.position = startPos;
        //scriptVar.enabled=true;
        scriptVar.Reset();

    }

    IEnumerator TurnOffTrail(int ballNumber)
    {
        yield return new WaitForSeconds(0.5f);
        switch (ballNumber)
        {
            case 1:
                //ball.transform.FindChild("Trail").gameObject.SetActive(false);
                ball1Trail.SetActive(false);
                break;
            case 2:
                //ball2.transform.FindChild("Trail").gameObject.SetActive(false);
                ball2Trail.SetActive(false);
                break;
            case 3:
                //ball3.transform.FindChild("Trail").gameObject.SetActive(false);
                ball3Trail.SetActive(false);
                break;
        }
    }
    IEnumerator GameTimer()
    {
        currentGameSeconds = secondsPerGame;
        while (currentGameSeconds >= 0)
        {
            if (!isPaused)
                currentGameSeconds--;
            if (currentGameSeconds >= 0 && (menu.gameType == Menu.GameType.Time15 || menu.gameType == Menu.GameType.Time30))
                counterLabel.text = currentGameSeconds.ToString() + " SEC";

            if (currentGameSeconds == 12)
                yield return new WaitForSeconds(1);
        }

        EndGame();
    }
    IEnumerator ActivateLightsCoRoutine(float waitTime)
    {
        ActivateLights();
        yield return new WaitForSeconds(waitTime);
        DeactivateLights();
    }

    string FormatScore(float score)
    {
        float million = 1000000;
        float million10 = 10000000;
        float billion = 1000000000;
        float trillion = 1000000000000;
        float quadrillion = 1000000000000000;

        string formatted;
        int maxChars = 6;
        //float dividedScore;
        //float score = (float)score; 
        if (score > quadrillion)
        {
            formatted = ConvertScoreToTruncatedString(score, quadrillion, maxChars, "Q");
        }
        else if (score > trillion)
        {
            formatted = ConvertScoreToTruncatedString(score, trillion, maxChars, "T");
        }
        else if (score > billion)
        {
            formatted = ConvertScoreToTruncatedString(score, billion, maxChars, "B");
        }
        else if (score > million10)
        {
            formatted = ConvertScoreToTruncatedString(score, million, maxChars, "M");
        }
        else
        {
            formatted = score.ToString();
        }
        return formatted;
    }

    string Truncate(string value, int maxLength)
    {
        if (string.IsNullOrEmpty(value)) return value;
        return value.Length <= maxLength ? value : value.Substring(0, maxLength);
    }

	IEnumerator DoStartupSounds(){
		gameLoopMusic.gameObject.SetActive (false);
		gameIntroMusic.volume = Settings.instance.musicVolume; 
		gameIntroSound.volume = Settings.instance.soundVolume; 
        //Debug.Log("Playing music vol=" + gameIntroMusic.volume + ", settingVol=" + Settings.instance.musicVolume);
		if(!gameIntroMusic.isPlaying&&!gameIntroSound.isPlaying)
			gameIntroMusic.Play (); 
		//gameIntroMusic.PlayOneShot (gameIntroMusic.clip, Settings.instance.musicVolume); 
		yield return new WaitForSeconds(gameIntroMusic.clip.length);
		if (!gameIntroSound.isPlaying&&!gameIntroMusic.isPlaying)
			gameIntroSound.Play (); 
		yield return new WaitForSeconds (gameIntroSound.clip.length); 
		gameLoopMusic.gameObject.SetActive (true); 
		
	}


    void EndGame()
    {
        Screen.sleepTimeout = SleepTimeout.SystemSetting;
        if (inGame)
        {
            ActivateLights();
            launcher.isEnabled = false;
            StartCoroutine(DisableFlippers());
            gameLoopMusic.gameObject.SetActive(false);
            //cameraControl.currentState = CameraControl.State.View2_ZoomedIn; //Zoom in to the ball.
            int place = leaderboard.GetPlace(score, settings.gameType);
            leaderboardEntryUI.Show(place, FormatScore(score));
            cameraButton.SetActive(false);
            pauseButton.SetActive(false);
            inGame = false;
        }
    }

    IEnumerator DisableFlippers()
    {
        uint i;
        LeftFlipper leftComp;
        RightFlipper rightComp;

        for (i = 0; i < leftFlippers.Length; i++)
        {
            leftComp = leftFlippers[i].transform.FindChild("Flipper_LP").GetComponent<LeftFlipper>();
            leftComp.isEnabled = false;
        }
        for (i = 0; i < rightFlippers.Length; i++)
        {
            rightComp = rightFlippers[i].transform.FindChild("Flipper_LP").GetComponent<RightFlipper>();
            rightComp.isEnabled = false;
        }
        
        yield return new WaitForSeconds(5);

        for (i = 0; i < leftFlippers.Length; i++)
        {
            leftComp = leftFlippers[i].transform.FindChild("Flipper_LP").GetComponent<LeftFlipper>();
            leftComp.isEnabled = true;

        }
        for (i = 0; i < rightFlippers.Length; i++)
        {
            rightComp = rightFlippers[i].transform.FindChild("Flipper_LP").GetComponent<RightFlipper>();
            rightComp.isEnabled = true;
        }
    }


    IEnumerator GetReadyAfterPause()
    {

        yield return StartCoroutine(cameraControl.DoResumeView());

        if (!isNewGame)
            yield return StartCoroutine("DoGetReadyText");

        for (int i = 0; i < balls.Length; i++)
        {
            balls[i].GetComponent<Rigidbody>().isKinematic = false;
            balls[i].GetComponent<Rigidbody>().velocity = storedVelocities[i];
            balls[i].GetComponent<Rigidbody>().angularVelocity = storedAngularVelocities[i];

            balls[i].GetComponent<Rigidbody>().WakeUp();

        }


        isNewGame = false;


    }

    IEnumerator DoGetReadyText()
    {
		getReady.text = "GET READY...";
        getReady.gameObject.SetActive(true);
        yield return new WaitForSeconds(2f);
        getReady.text = "GO!";
        yield return new WaitForSeconds(0.5f);
        getReady.gameObject.SetActive(false);

        isPausing = false;
		InputHelper.isEnabled = true; 

	}

	void OnApplicationPause()
	{
		if(inGame)
			Pause ();
	}

	void OnApplicationQuit()
	{
		Resources.UnloadUnusedAssets ();
		System.GC.Collect (); 
	}

	void DoTilt(GameObject ball)
	{
		Debug.Log ("ballHit"); 
		float posOrNeg = UnityEngine.Random.Range (0, 1) < 0.5 ? -1 : 1;
			if(ball.GetComponent<Rigidbody>().velocity.magnitude <= 1.5f&&!launcher.isTouchingLauncher)
			{
				ball.GetComponent<Rigidbody>().AddForce(new Vector3(UnityEngine.Random.Range(75, 125)*posOrNeg, 0f, UnityEngine.Random.Range(75, 125)*posOrNeg));
			}


	}

}
