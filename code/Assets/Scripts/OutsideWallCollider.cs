﻿using UnityEngine;
using System.Collections;

public class OutsideWallCollider : MonoBehaviour {

	public GameFlow gameFlow;
	public GameObject[] balls;
	private Collider other;

	void OnCollisionEnter(Collision collisionInfo){
		other = collisionInfo.collider;
		//if the colliding object is the ball , add some points to the score
		if((other.gameObject.name=="Ball")||(other.gameObject.name=="Ball2")||(other.gameObject.name=="Ball3")){
			
			if(gameFlow){
				gameFlow.ForceRespawn();
			}
		}
	}
}
