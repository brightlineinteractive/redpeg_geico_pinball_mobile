﻿using UnityEngine;
using System.Collections;

public class BallSafety : MonoBehaviour {

	public GameObject[] balls;
	public GameFlow gameFlow;
	void Start () {
		StartCoroutine(CheckBalls());
	}

	public IEnumerator CheckBalls(){
		while(true){
			yield return new WaitForSeconds(1);
			for(int ball =0; ball<balls.Length; ball++){
				//Debug.Log(balls[ball].rigidbody.worldCenterOfMass);
				if((balls[ball].GetComponent<Rigidbody>().worldCenterOfMass.y<-10)||(balls[ball].GetComponent<Rigidbody>().worldCenterOfMass.z>10)){
					balls[ball].SetActive(false);
					if(gameFlow){
						gameFlow.ForceRespawn();
					}
				}
			}
		}
	}
}
