﻿using UnityEngine;
using System.Collections;

public class ScoreAnimCollider : MonoBehaviour {

	ScoreboardAnimationHandler scoreAnim; 

	void Start()
	{
		scoreAnim = GameObject.Find ("ScoreboardAnimationHandler").GetComponent<ScoreboardAnimationHandler> (); 
	}

	void OnTriggerEnter(Collider collider)
	{
		//scoreAnim.Animate (scoreAnim.camel); 
		if(collider.gameObject.CompareTag("Ball"))
			scoreAnim.SelectAnimation (this.GetComponent<Collider>()); 
	}
}
