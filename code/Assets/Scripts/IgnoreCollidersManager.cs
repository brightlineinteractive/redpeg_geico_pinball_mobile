﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ColliderPair
{
	public Collider a;
	public Collider b;
}

public class IgnoreCollidersManager : MonoBehaviour {
	
	public ColliderPair[] colliderPairs = new ColliderPair[5];
	private Rigidbody[] rbs;

	// Use this for initialization
	void Start () 
	{
		StartCoroutine (IgnoreColliders ());
		rbs = FindObjectsOfType<Rigidbody> ();
	}

	private IEnumerator IgnoreColliders()
	{
		yield return null;

		if (colliderPairs.Length < 1)
			Destroy (this.gameObject);

		for (int i = 0; i < colliderPairs.Length; i++)
		{
			Physics.IgnoreCollision(colliderPairs[i].a,colliderPairs[i].b);
		}

		//Destroy (this.gameObject);
	}
}
