﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

public class TurnOffColliders : MonoBehaviour {

	Collider[] Colliders; 
	Transform Trans; 
	float Distance = 50; 

	// Use this for initialization
	void Start () 
	{
		Colliders = GameObject.FindObjectsOfType (typeof(Collider)) as Collider[]; 
		Trans = this.transform; 
	}

	void Update()
	{
		for (int i=0; i<Colliders.Length; i++) 
		{
			Vector3 V3; 
			float MagnitudeSquared; 
			V3 = Colliders[i].gameObject.transform.position - Trans.position; 
			MagnitudeSquared = V3.sqrMagnitude; 

			if(MagnitudeSquared > Distance)
			{
				Colliders[i].enabled=false; 
			}
			else
			{
				Colliders[i].enabled=true; 
			}

		}
	}
	


}
