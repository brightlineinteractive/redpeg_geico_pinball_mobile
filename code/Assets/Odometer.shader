// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Shader created with Shader Forge Alpha 0.14 
// Shader Forge (c) Joachim 'Acegikmo' Holmer
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.14;sub:START;pass:START;ps:lgpr:1,nrmq:1,limd:2,blpr:1,bsrc:3,bdst:7,culm:0,dpts:2,wrdp:True,uamb:True,ufog:True,aust:True,igpj:False,qofs:0,lico:1,qpre:2,flbk:,rntp:3,lmpd:False,enco:False,frtr:True,vitr:True,dbil:False;n:type:ShaderForge.SFN_Final,id:1,x:32719,y:32712|diff-342-OUT,clip-187-OUT;n:type:ShaderForge.SFN_Tex2d,id:186,x:33633,y:32719,ptlb:odometer,tex:907415ad43843453494723f026481b9c|UVIN-189-UVOUT;n:type:ShaderForge.SFN_ComponentMask,id:187,x:33452,y:32756,cc1:3,cc2:4,cc3:4,cc4:4|IN-186-R;n:type:ShaderForge.SFN_TexCoord,id:188,x:33699,y:33110,uv:0;n:type:ShaderForge.SFN_Panner,id:189,x:33041,y:33059,spu:0,spv:2|UVIN-335-OUT,DIST-236-OUT;n:type:ShaderForge.SFN_Slider,id:236,x:33552,y:33301,ptlb:node_236,min:0.5,cur:0.7105263,max:0.75;n:type:ShaderForge.SFN_Tex2d,id:245,x:33417,y:32589,ptlb:odometer2,tex:634095096811f4e1e82b738df2623f02|UVIN-331-UVOUT;n:type:ShaderForge.SFN_Slider,id:329,x:33799,y:32661,ptlb:node_329,min:0,cur:0,max:1;n:type:ShaderForge.SFN_TexCoord,id:330,x:33969,y:32413,uv:0;n:type:ShaderForge.SFN_Panner,id:331,x:33687,y:32446,spu:0,spv:1|UVIN-330-UVOUT,DIST-329-OUT;n:type:ShaderForge.SFN_Multiply,id:335,x:33432,y:32976|A-188-UVOUT,B-337-OUT;n:type:ShaderForge.SFN_Vector2,id:337,x:33718,y:32992,v1:0.5,v2:0.5;n:type:ShaderForge.SFN_Multiply,id:338,x:33190,y:32672|A-245-RGB,B-187-OUT;n:type:ShaderForge.SFN_Multiply,id:339,x:33147,y:32482|A-245-RGB,B-340-RGB;n:type:ShaderForge.SFN_Color,id:340,x:33417,y:32404,ptlb:node_340,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Add,id:342,x:32947,y:32636|A-339-OUT,B-338-OUT;proporder:186-236-245-329;pass:END;sub:END;*/

Shader "Shader Forge/Odometer" {
    Properties {
        _odometer ("odometer", 2D) = "white" {}
        _node236 ("node_236", Range(0.5, 0.75)) = 0.5
        _odometer2 ("odometer2", 2D) = "white" {}
        _node329 ("node_329", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers gles xbox360 ps3 flash 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _odometer;
            uniform float _node236;
            uniform sampler2D _odometer2;
            uniform float _node329;
            uniform float4 _node340;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normalDir = mul(float4(v.normal,0), unity_WorldToObject).xyz;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float node_187 = tex2D(_odometer,((i.uv0.rg*float2(0.5,0.5))+_node236*float2(0,2))).r;
                clip(node_187 - 0.5);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = normalize(i.normalDir);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 halfDirection = normalize(viewDirection+lightDirection);
                float attenuation = LIGHT_ATTENUATION(i);
                float3 lambert = max( 0.0, dot(normalDirection,lightDirection ));
                lambert *= _LightColor0.xyz*attenuation;
                float3 lightFinal = lambert + UNITY_LIGHTMODEL_AMBIENT.xyz;
                float4 node_245 = tex2D(_odometer2,(i.uv0.rg+_node329*float2(0,1)));
                return fixed4(lightFinal * ((node_245.rgb*_node340.rgb)+(node_245.rgb*node_187)),1);
            }
            ENDCG
        }
        Pass {
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma exclude_renderers gles xbox360 ps3 flash 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _odometer;
            uniform float _node236;
            uniform sampler2D _odometer2;
            uniform float _node329;
            uniform float4 _node340;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normalDir = mul(float4(v.normal,0), unity_WorldToObject).xyz;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float node_187 = tex2D(_odometer,((i.uv0.rg*float2(0.5,0.5))+_node236*float2(0,2))).r;
                clip(node_187 - 0.5);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = normalize(i.normalDir);
                float3 lightDirection;
                if (0.0 == _WorldSpaceLightPos0.w){
                    lightDirection = normalize( _WorldSpaceLightPos0.xyz );
                } else {
                    lightDirection = normalize( _WorldSpaceLightPos0 - i.posWorld.xyz );
                }
                float3 halfDirection = normalize(viewDirection+lightDirection);
                float attenuation = LIGHT_ATTENUATION(i);
                float3 lambert = max( 0.0, dot(normalDirection,lightDirection ));
                lambert *= _LightColor0.xyz*attenuation;
                float3 lightFinal = lambert;
                float4 node_245 = tex2D(_odometer2,(i.uv0.rg+_node329*float2(0,1)));
                return fixed4(lightFinal * ((node_245.rgb*_node340.rgb)+(node_245.rgb*node_187)),1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCollector"
            Tags {
                "LightMode"="ShadowCollector"
            }
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCOLLECTOR
            #define SHADOW_COLLECTOR_PASS
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcollector
            #pragma exclude_renderers gles xbox360 ps3 flash 
            #pragma target 3.0
            uniform sampler2D _odometer;
            uniform float _node236;
            struct VertexInput {
                float4 vertex : POSITION;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_COLLECTOR;
                float4 uv0 : TEXCOORD5;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_COLLECTOR(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float node_187 = tex2D(_odometer,((i.uv0.rg*float2(0.5,0.5))+_node236*float2(0,2))).r;
                clip(node_187 - 0.5);
                SHADOW_COLLECTOR_FRAGMENT(i)
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Cull Off
            Offset 1, 1
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers gles xbox360 ps3 flash 
            #pragma target 3.0
            uniform sampler2D _odometer;
            uniform float _node236;
            struct VertexInput {
                float4 vertex : POSITION;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float4 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float node_187 = tex2D(_odometer,((i.uv0.rg*float2(0.5,0.5))+_node236*float2(0,2))).r;
                clip(node_187 - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
}
