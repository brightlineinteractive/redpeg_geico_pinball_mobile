﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class PulseFadeAlpha : MonoBehaviour
{
    public float AlphaMin = 0;

    public float CycleTimeSecs = 1;

    float amt = 0;
    Color c = Color.white;

    // Update is called once per frame
    void Update()
    {
        amt += Time.deltaTime / CycleTimeSecs;
        amt %= 1;

        if (amt < 0.5f)
            c.a = amt * 2;
        else
            c.a = 1 - 2 * (amt - 0.5f);

        GetComponent<Image>().color = c;
    }
}
