﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelLoader : MonoBehaviour
{
    public string MainSceneName;
    public float LoadDelaySecs;
    public Slider SliderProgressBar;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(doLoading());
    }

    IEnumerator doLoading()
    {
        yield return new WaitForSeconds(LoadDelaySecs);

        var async =  Application.LoadLevelAsync(MainSceneName);

        if(SliderProgressBar == null)
            yield break;

        do
        {
            SliderProgressBar.value = async.progress;
            yield return null;

        } while (!async.isDone);

        SliderProgressBar.value = 1;
    }

}
